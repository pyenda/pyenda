#!/usr/bin/env python
# -*- coding: utf-8 -*-

# authors:
# * Merker, Claire <claire.merker@uni-hamburg.de>
# * Geppert, Gernot <gernot.geppert@uni-hamburg.de>


"""
Data Assimilation Experiment
----------------------------

main program for data assimilation-forecasts cycles

Example
-------
Call

    $ python assimilate.py [options] <config_file>

Options:

config_file: configuration file path+name, default is './config/assimilate_exp.yml'

-o, --overwrite_files Overwrite output files

-s, --seed <seed> Int, specified seed for random processes

-np, --processors <number> Int, number of used processors for parallelisation, Default is 0

-v, --verbose Generate command line output

-d, --debug Generate detailled command line output for debugging

Version
-------
* 2015-11-12 - initial version
* 2016-01-05 - moved work to main() function;
               enabled assimilation of different obs types
* 2016-08-03 - moved observation creation to function in enkf/observation_set.py module
* 2016-11-07 - renamed package 'enkf' to 'pyenda'
"""


from pyenda import da_filter
from pyenda.observation import (Observation)
from pyenda.ensemble import (ObservationEnsemble,
                             ModelEnsemble)
from pyenda.file_writer import (EnsembleWriter, FlaggedEnsembleWriter)
from pyenda.observation_set import (create_observation_set,
                                    obs_generator)

from os import remove, path
import yaml
import numpy as np
import datetime
import argparse as ap
import logging


def main(config_file, overwrite_files, processors):
    ###########################################################################
    ### read parameters and settings
    ###########################################################################
    ### read in data assimilation cycle parameter dict from config/<file>.yml
    with open(config_file, 'r') as ymlfile:
        cfg = yaml.load(ymlfile)
        cfg['da_method_args'] = cfg.pop('da_method_args', [])
    ### read in observation sets dict from observations/<file>.yml
    with open(cfg['observations'], 'r') as ymlfile:
        obs_sets_dict = yaml.load(ymlfile)
    ### read in forecast model parameter dict from config/<file>.yml
    try:
        with open(cfg['model_config'], 'r') as ymlfile:
            model_cfg = yaml.load(ymlfile)
    except TypeError:
        model_cfg = {}
    ### add initialising time to model_cfg if not existent
    model_cfg['t_init'] = model_cfg.pop('t_init', cfg['t_start'])

    ### check if output files exist and overwrite or not
    files = [cfg['da_output']]
    if cfg['aux_da_output'] is not None:
        files = files + [cfg['aux_da_output']]
    if cfg['forecast_only'] is False and cfg['write_obs_ens'] is True:
        files = files + [d['obs_ens_out_file'] for d in obs_sets_dict['observations']]
    if overwrite_files:
        for filename in files:
            ### logging info
            logging.info('remove %s...', filename)
            try:
                remove(filename)
            except OSError:
                pass
    else:
        for filename in files:
            if path.exists(filename):
                raise Exception("File {0} already exists.".format(filename))

    ###########################################################################
    ### initialise data assimilation method, forecast model and model ensemble
    ###########################################################################
    ### initialise da method
    ### logging info
    logging.info('initialise ' + cfg['da_method'])
    da_method = getattr(da_filter, cfg['da_method'])(*cfg['da_method_args'])
    ### initialise forecast model
    ### logging info
    logging.info('initialise ' + cfg['model_class'])
    model = __import__('models.' + cfg['model_package'],
                       globals(),
                       locals(),
                       [cfg['model_class']],
                       0)
    model = getattr(model, cfg['model_class'])

    ### initialise forecast ensemble
    ensemble = ModelEnsemble("state ensemble",
                             cfg['ensemble_size'],
                             model,
                             model_cfg,
                             processors
                             )

    ###########################################################################
    ### prepare netcdf output
    ###########################################################################
    ### create netcdf file for main output and write first time step
    nc_file = FlaggedEnsembleWriter(cfg['da_output'],
                                    model.variables,
                                    model.dimensions,
                                    ensemble.size,
                                    add_time_dim=True,
                                    add_coord_dims=model.locations.coord_dims
                                    )
    nc_file.write_variables(ensemble.deconstruct(),
                            cfg['t_start'],
                            0)
    nc_file.write_locations(model.locations)

    ### create netcdf file for auxiliary output and write first time step
    ### this file will only contain forecast steps since variables are not used
    ### for data assimilation, analysis time steps would be redundant.
    if cfg['aux_da_output'] is not None:
        aux_nc_file = EnsembleWriter(cfg['aux_da_output'],
                                     model.aux_variables,
                                     model.aux_dimensions,
                                     ensemble.size,
                                     add_time_dim=True,
                                     add_coord_dims=model.aux_locations.coord_dims
                                     )
        aux_nc_file.write_variables(ensemble.aux_deconstruct(),
                                    cfg['t_start'])
        aux_nc_file.write_locations(model.aux_locations)

    ### prepare observation sets when running assimilation
    if not cfg['forecast_only']:
        ### logging info
        logging.info('prepare %s observation sets', len(obs_sets_dict['observations']))
        #######################################################################
        ### prepare observation set for data assimilation
        ### an observation set can be either a single observation or a set of
        ### repeated same measurements
        #######################################################################
        ### store observations in list
        obs_sets = []
        for obs_set_dict in obs_sets_dict['observations']:
            logging.debug('create obs set')
            obs_sets.append(
                create_observation_set(obs_set_dict,
                                       model,
                                       write_obs_ensemble=cfg['write_obs_ens'],
                                       ensemble_size=ensemble.size))

    ###########################################################################
    ### assimilation-forecast cycle
    ###########################################################################
    ### starting time
    t = cfg['t_start']
    ### set last_t_obs to time of first observation
    if not cfg['forecast_only']:
        ### get first observation time and assign as starting value to last_t_obs
        ind = np.argmin(np.array([obs_set.next_obs_time for obs_set in obs_sets]))
        last_t_obs = obs_sets[ind].next_obs_time

        ### loop through observations
        #import pdb; pdb.set_trace()
        for t_obs, obs_set in obs_generator(obs_sets, cfg['t_end']):
            if t_obs < t:
                continue
            ### write the last analysis to netcdf file if the next observation
            ### is at a later time
            if t_obs > last_t_obs:
                nc_file.write_variables(ensemble.deconstruct(),
                                        t, 1)
            ### forecast until next observation time is reached
            while t < t_obs:
                ensemble.forecast(t, cfg['t_step'])
                ### logging info
                logging.info('model forecast %s', t+cfg['t_step'])

                t += cfg['t_step']
                ### write forecast to main output
                nc_file.write_variables(ensemble.deconstruct(),
                                        t, 0)
                ### write forecast to auxiliary output
                if cfg['aux_da_output'] is not None:
                    aux_nc_file.write_variables(ensemble.aux_deconstruct(),
                                                t)

            ### logging info
            logging.info('analysis step  %s', t)

            ### instantiate observation object
            obs = Observation(t_obs, obs_set.operator,
                              locations=obs_set.locations,
                              domains=obs_set.domains
                              )
            ### read observation at observation time
            obs.read_obs_from_file(t_obs, obs_set.filename, obs_set.file_ind,
                                   obs_set.locations, obs_set.cov_filename)
            ### create observation equivalent ensemble
            obs_ensemble = ObservationEnsemble(
                'observation ensemble',
                obs.operator.calc_obs_ensemble(ensemble, obs_set.state_ind),
                obs_set.operator)
            ### perform analysis
            da_method.update(ensemble, obs, obs_ensemble)
            ### write observation ensemble
            obs_set.write(obs_ensemble, t)
            ### update last observation time
            last_t_obs = t_obs

        ### write the very last analysis to netcdf file
        nc_file.write_variables(ensemble.deconstruct(),
                                last_t_obs, 1)

    ### if no observation was found in the sequence file within the forecast
    ### period (len(t_obs) is 0), or if the time of the last observation is
    ### before the end of the forecast period, the forecast is performed until
    ### the end of the forecast period
    ###
    ### forecast until end of forcast period
    while t < cfg['t_end']:
        ### logging info
        logging.info('model forecast %s', t+cfg['t_step'])

        ensemble.forecast(t, cfg['t_step'])
        t += cfg['t_step']
        ### write forecast to main output
        nc_file.write_variables(ensemble.deconstruct(), t, 0)
        ### write forecast to auxiliary output
        if cfg['aux_da_output'] is not None:
            aux_nc_file.write_variables(ensemble.aux_deconstruct(), t)

    ### close netcdf file
    nc_file.close_file()
    if cfg['aux_da_output'] is not None:
        aux_nc_file.close_file()


if __name__ == '__main__':
    parser = ap.ArgumentParser()
    parser.add_argument("config_file", nargs='?', default="./config/assimilate_exp.yml")
    parser.add_argument("--overwrite_files", "-o", action='store_true')
    parser.add_argument("--seed", "-s", nargs='?', type=int, const=0, default=None)
    parser.add_argument("--processors", "-np", nargs='?', type=int, const=1, default=0)
    parser.add_argument("--verbose", "-v", action="store_const", dest="log_level",
                        const=logging.INFO, default=logging.WARNING)
    parser.add_argument("--debug", "-d", action="store_const", dest="log_level",
                        const=logging.DEBUG)
    args = parser.parse_args()

    ### configure logging module
    logging.basicConfig(format='%(levelname)s:   %(message)s',
                        level=args.log_level)
    ### initialise seed for random processes
    np.random.seed(args.seed)

    print('')
    print('============ PY_ENDA START: ' + str(datetime.datetime.now()) + ' =============')
    main(args.config_file, args.overwrite_files, args.processors)
    print('============ PY_ENDA END: ' + str(datetime.datetime.now()) + ' =============')
