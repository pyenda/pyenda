===============
pyenda examples
===============

Lorenz '96
==========
This example generates synthetic observations from the 40-variable
Lorenz '96 model and assimilates them into a 10-member ensemble.

The file ``config_generate_l96.yml`` contains the settings for the run
to create the observations and the file ``obslist_generate_l96.yml``
contains a descriptions of the observations which shall be generated.

To generate the observations, ``generate_obs.py`` is called with the
settings as its argument: ::
  ./generate_obs.py doc/examples/config_generate_l96.yml
This generates a list of observations to be assimilated
(``obslist_assimilate_l96.yml``), the observations (``l96_obs.nc``),
and the true state (``l96_truestate.nc``).

To generate new observations after some changes to
``obslist_generate_l96.yml`` or ``config_generate_l96.yml``, use the
``-o`` flag to overwrite existing files. Additional output can be
switched on by using ``-v`` or ``-d``: ::
  ./generate_obs.py -o -d doc/examples/config_generate_l96.yml

To assimilate the newly created observations, ``assimilate.py`` is
called with the settings for assimilation in
``config_assimilate_l96.yml`` as its argument: ::
  ./assimilate.py doc/examples/config_assimilate_l96.yml
This generates a file with the assimilation results
(``l96_results.nc``) as well as a file with the ensembles of
observation equivalents (``ens_out_l96_obs.nc``).

To repeat the experiment with modified settings and overwrite existing
files, use ``-o``. To add additional output, use ``-v`` or ``-d``.
