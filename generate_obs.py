#!/usr/bin/env python
# -*- coding: utf-8 -*-

# authors:
# * Merker, Claire <claire.merker@uni-hamburg.de>
# * Geppert, Gernot <gernot.geppert@uni-hamburg.de>


"""
Synthetic Observation Generator
-------------------------------

Generate true state and observations from forecast model

Example
-------
Call

    $ python synthetic_obs.py [options] <config_file>

Options:

config_file: configuration file name, default is 'config.yml'

-o, --overwrite_files Overwrite output files

-s, --seed <seed> Int, specified seed for random processes

-v, --verbose Generate command line output

Version
-------
* 2015-12-21 - initial version (linsystem_truth_obs.py);
* 2016-01-04 - made the script applicable to general forecast model;
               added configurable error models;
"""

from pyenda.file_writer import Writer, datetime_to_unixtime, Variable
from pyenda import location
from pyenda.observation import _expand_coordinates, _parse_slice, _make_int_slice
from pyenda.observation_set import SyntheticSet, obs_generator, parse_obs_time
from pyenda import observation_operator
import yaml
import numpy as np
from os import remove, path
import argparse as ap
import netCDF4 as nc
import logging

def get_cov_info(obs_size, error_model, error_model_args):
    """
    Returns
    -------
    tuple
        Tuple of dimension names for 'obs_covariance'.
    List[Tuple]
        List of tuples with dimension name and size.
    np.ndarray
        observation error covariance if constant in time, otherwise None
    """
    ### for uncorrelated errors
    if error_model in ['add_uncorr_gauss_error', 'add_uncorr_beta_error']:
        ### for constant covariance, just save the number
        covar = np.asarray(error_model_args).flatten()
        if covar.shape[0] == 1:
            return (), [], covar
        ### else save a vector with one value for each obs, ie. the diagonal of
        ### the cov matrix, but constant in time
        else:
            return ('obs_index',), [('obs_index', obs_size)], covar
    ### for correlated errors save the whole matrix, eg. like this
    # elif error_model in ['correlated']:
    #     return ('obs_index', 'obs_index'), [('obs_index', operator.size)], covar
    ### for time dependent, uncorrelated errors, return covar=None, eg.
    # elif error_model in ['time_dependent']:
    #     return ('time', 'obs_index'), [('time', None), ('obs_index', operator.size)], None
    else:
        raise Exception("Error model {0} not implemented.".format(error_model))


def get_next_obs(obs_sets, obs_set_dicts):
    ind = np.argmin(np.array([obs_set.next_obs_time for obs_set in obs_sets]))
    return obs_sets[ind], obs_set_dicts[ind]


def _parse_obs_location(loc_dict):
    """
    Returns
    -------
    locations : Location object
        instance of the location class
    """

    # to avoid changes in loc_dict outside of the function
    loc_dict = loc_dict.copy()
    _ = loc_dict.pop('max_offset', None)
    loc_kw_args = loc_dict.pop('loc_type_args', {})
    loc_class = getattr(location, loc_dict.pop('loc_type'))

    #_ = loc_dict.pop('assim_slices', None)
    try:
        loc_fid = nc.Dataset(loc_dict['file'])
        coords = _expand_coordinates(loc_fid, loc_dict['var_name'],
                                     loc_dict['coord_names'])
        coord_slice = []
        for s in loc_dict['slices'][1]:
            coord_slice.append(_make_int_slice(_parse_slice(s)))

        locations = loc_class(
                        *[np.asarray(coord)[coord_slice] for coord in coords],
                        **loc_kw_args)

    except KeyError:
        locations = loc_class(**loc_dict, **loc_kw_args)

    # set coord dim names to default values (lower case coord var names)
    # or the names given in the config file

    # second out var is list of dim tuples
    return locations


def main(config_file, overwrite_files):
    ###########################################################################
    ### read parameters and settings
    ###########################################################################
    ### read parameters from config.yml using yaml
    with open(config_file, 'r') as ymlfile:
        cfg = yaml.load(ymlfile)
    ### read parameters containing observation information
    with open(cfg['observations_in'], 'r') as ymlfile:
        obs_dict = yaml.load(ymlfile)
    ### read in forecast model parameter dict from config/<file>.yml
    try:
        with open(cfg['model_config'], 'r') as ymlfile:
            model_cfg = yaml.load(ymlfile)
    except TypeError:
        model_cfg = {}
    ### add initialising time to model_cfg if not existent
    model_cfg['t_init'] = model_cfg.pop('t_init', cfg['t_start'])

    ### get file names and information about wanted observation sets
    files = [cfg['true_file'], cfg['observations_out']]
    obs_set_dicts = obs_dict.pop('observations', [])

    ### overwrite files or not
    if overwrite_files:
        for obs_set_dict in obs_set_dicts:
            files.append(obs_set_dict['file'])
        for filename in files:
            logging.info("Remove {0} ...".format(filename))
            try:
                remove(filename)
            except OSError:
                pass
    else:
        for filename in files:
            if path.exists(filename):
                raise Exception("File {0} already exists.".format(filename))

    ###########################################################################
    ### initialise forecast model
    ###########################################################################

    ### initialise model and get locations of state vector used for assimilation
    model_pkg = __import__('models.' + cfg['model_package'],
                       globals(),
                       locals(),
                       [cfg['model_class']],
                           0)
    ### get model class and use it to retrieve locations
    model_cls = getattr(model_pkg, cfg['model_class'])

    ### now creat model instance
    model = model_cls(model_cfg)
    model.init_model(model_cfg)

    ### then get the model locations
    model_locations = model_cls.locations


    logging.debug('make_synthetic_obs, coord_dims, {0}'.format(model_locations.coord_dims))
    ###########################################################################
    ### prepare netcdf output
    ###########################################################################
    ### initialise file writers, observation operators, observation times
    true_writer = Writer(cfg['true_file'],
                         model.variables,
                         model.dimensions,
                         add_time_dim=True,
                         add_coord_dims=model_locations.coord_dims)
    true_writer.write_locations(model_locations)
    ###########################################################################
    ### create observation sets structure and write information about
    ### observation sets to pyyaml config file for data assimilation cycle
    ### (da_exp.py)
    ###########################################################################
    obs_sets = []
    ### new dict for observation sequence file for data assimilation cycle
    ### (da_exp.py)
    obs_out = {}
    obs_out['observations'] = []
    ### loop through wanted observations
    for obs_set_dict in obs_set_dicts:
        ### get observation operator, locations, covariance information
        current_operator = getattr(observation_operator, obs_set_dict['operator'])()
        current_locations = _parse_obs_location(obs_set_dict['location'])
        cov_dim_names, cov_dims, covar = get_cov_info(np.array(current_locations.shape).prod(),
                                                      obs_set_dict['error']['model'],
                                                      obs_set_dict['error']['args'])

        ### prepare netcdf output for covariance information
        ### Writer must have dimensions matching the locations of the obs_set!
        current_writer = Writer(obs_set_dict['file'],
                                current_operator.variables,
                                current_operator.dimensions,
                                add_time_dim=True,
                                add_coord_dims=current_locations.coord_dims
                                )
        current_writer.add_dimensions(cov_dims)
        current_writer.add_variables([Variable('obs_covariance',
                                               'd',
                                               cov_dim_names, ['-'])])
        ### if obs error covariance is constant, write to file
        if 'time' not in [dim[0] for dim in cov_dims]:
            current_writer._fid['obs_covariance'][:] = covar
        current_writer.write_locations(current_locations)

        ### create synthetic observation set
        current_obs_set = SyntheticSet(current_operator, current_writer,
                                       parse_obs_time(obs_set_dict['time']),
                                       current_locations, current_locations.shape,
                                       obs_set_dict['error']['model'],
                                       obs_set_dict['error']['args']
                                       )
        ### append to list
        obs_sets.append(current_obs_set)
        _ = obs_set_dict.pop('error')

        ### set observation files
        obs_set_dict['cov_file'] = obs_set_dict['file']
        filename_parts = obs_set_dict['file'].split('/')
        filename_parts[-1] = "ens_out_" + filename_parts[-1]
        obs_set_dict['obs_ens_out_file'] = '/'.join(filename_parts)

        ### set data array slice type and values
        ### default ':' selects all


###################### CHANGE the way slice type and slices are written to obs_assimilate.yml
###################### to be consistent with what is expected there


        try:
            obs_set_dict['location']['slice_type'] = obs_set_dict['assim_slice_type']
            obs_set_dict['location']['slices'] = obs_set_dict['assim_slices']
        except KeyError:
            obs_set_dict['location']['slice_type'] = 'index'
            obs_set_dict['location']['slices'] = [":"]

        try:
            obs_set_dict['time'] = obs_set_dict.pop('assim_time')
        except:
            pass

        ### set synthetic observation location information
        obs_set_dict['location'] = {
            'loc_type': obs_set_dict['location']['loc_type'],
            'coord_names': current_locations.var_names,
            'slice_type': obs_set_dict['location']['slice_type'],
            'slices': obs_set_dict['location']['slices'],
            'max_offset': obs_set_dict['location']['max_offset'],
            'loc_type_args': obs_set_dict['location'].pop('loc_type_args', {})
            }
        if obs_set_dict['location']['loc_type'] == 'IndexLocation':
            obs_set_dict['location']['state_index'] = 'state_index'

        ### append new synthetic observation
        obs_out['observations'].append(obs_set_dict)

    ### write observation information file for data assimilation cycle
    ### (assimilate.py)
    with open(cfg['observations_out'], 'w') as ymlfile:
        yaml.dump(obs_out, ymlfile, default_flow_style=False)

    ###########################################################################
    ### create synthetic observations from forecast run
    ###########################################################################
    ### initialise time
    t = cfg['t_start']
    ### write starting point
    true_writer.write_variables(model.deconstruct(), t)

    ### loop through observation sets and observation times
    ### replace this with for obs in obs_iter where obs_iter cycles through the
    ### time iterators of the obs sets
    for current_obs_time, current_obs_set in obs_generator(obs_sets, cfg['t_end']):
        ### forecast until next observation time
        while t < current_obs_time:
            ### logging info
            logging.info('model forecast %s', t+cfg['t_step'])
            model.forecast(t, cfg['t_step'])
            t += cfg['t_step']
            true_writer.write_variables(model.deconstruct(), t)
        ### logging info
        logging.info('obervation "%s" %s', current_obs_set.operator.kind, t)
        ### save the shape of the locations as they will be converted to a
        ### 1d vector in the calc_obs call

        observation = current_obs_set.operator.calc_obs(model.get_filter_state(slice(None)),
                                                         model_locations,
                                                         current_obs_set.locations)

        observation, cov = current_obs_set.add_error(observation)

        ### write synthetic observation to netcdf file
        current_obs_set.write(observation, t)

        ### write covariance if it changes over time
        if 'time' in cov_dims:
            current_obs_set.write({'obs_covariance': cov}, t)

    ### close all synthetic observation files
    # for obs_set in obs_sets:
    #     obs_set.writer.close_file()

    ### forecast until end of forcast period
    while t < cfg['t_end']:
        ### logging info
        logging.info('model forecast %s', t+cfg['t_step'])
        model.forecast(t, cfg['t_step'])
        t += cfg['t_step']
        true_writer.write_variables(model.deconstruct(), t)

    ### close forecast file
    true_writer.close_file()


if __name__ == '__main__':
    parser = ap.ArgumentParser()
    parser.add_argument("config_file", nargs='?', default="config.yml")
    parser.add_argument("--overwrite_files", "-o", action='store_true')
    parser.add_argument("--seed", "-s", nargs='?', const=0, type=int, default=None)
    parser.add_argument("--verbose", "-v", action="store_const", dest="log_level",
                        const=logging.INFO, default=logging.WARNING)
    parser.add_argument("--debug", "-d", action="store_const", dest="log_level",
                        const=logging.DEBUG)
    args = parser.parse_args()

    ### configure logging module
    logging.basicConfig(format='%(levelname)s:   %(message)s',
                        level=args.log_level)

    np.random.seed(args.seed)

    main(args.config_file, args.overwrite_files)
