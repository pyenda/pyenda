!! Simple 1D-advection model on periodic domain

module Adv_mod
    implicit none

    ! grid specification
    integer, parameter          :: NX = 100   ! number of grid points in x
    double precision, parameter :: DX = 10.0 ! x grid width (metres)

    ! time variables
    integer, parameter :: NT   = 1000 ! number of times
    double precision   :: DT = 1.0  ! timestep (seconds)

    ! equation parameters
    double precision, parameter :: U0 = 1 ! advection (wind) velocity (m/s)

    ! model variables
    double precision :: T0 = 273.15        ! basic temperature
    double precision :: Tc(0:NX+1) = 0.    ! current temperature deviation

    ! loop variables
    integer :: i
end module Adv_mod

subroutine init
    use Adv_mod
    implicit none

    ! initialize with a gaussian in the middle
    do i=0,NX+1
        Tc(i) = exp(- (dble(i)-dble(NX)/2)**2 &
                      / (dble(NX)/8)**2 )
    end do
end subroutine init

subroutine step
    use Adv_mod
    implicit none

    ! euler in time
    ! upstream in space
    do i=1,NX+1 ! loop over all locations
        Tc(i) = Tc(i) - DT * U0 * (Tc(i) - Tc(i-1))/DX
    end do
end subroutine step

! boundary conditions
subroutine boundary
    use Adv_mod
    implicit none

    ! cyclic boundary conditions
    Tc(0) = Tc(NX+1)
end subroutine boundary
