from pyenda.forecast_model import ForecastModel
from pyenda.file_writer import Variable
from pyenda.location import Cartesian1DLocation
from . import advection as adv
import numpy as np


### the Advection class stores an instance's state in self.state since direclty writing
### to adv.adv_mod.tc is not possible because there is only once copy of adv with only
### one copy of adv.adv_mod.tc


class Advection(ForecastModel):
    kind = 'Advection'
    variables = [Variable('state', 'd', (), '-', digits=3)]
    filter_locations = Cartesian1DLocation(np.arange(0, adv.adv_mod.nx+2))

    def init_model(self, cfg):
        adv.init()
        adv.boundary()
        self.state = adv.adv_mod.tc

    @classmethod
    def init_ensemble(cls, cfg, size):
        perturb = np.random.normal(1, 0.2, 100)
        members = []
        adv.init()
        adv.boundary()
        for i in range(size):
            members.append(cls('member {0}'.format(i), cfg, initialize=False))
            members[-1].state = perturb[i] * adv.adv_mod.tc
        return members

    def forecast(self, t, dt):
        adv.adv_mod.dt = dt.total seconds()
        adv.adv_mod.tc[:] = self.state
        adv.step()
        adv.boundary()
        self.state[:] = adv.adv_mod.tc

    def deconstruct(self):
        return {'state': self.state}

    def get_filter_state(self, index=slice(None)):
        return self.state[index]

    def update_state(self, new_filter_state, index=slice(None)):
        self.state[index] = new_filter_state
