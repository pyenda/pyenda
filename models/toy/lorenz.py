from pyenda.forecast_model import ForecastModel
from pyenda.file_writer import Variable
from pyenda.location import PeriodicIndexLocation

from scipy import integrate
import numpy as np


class Lorenz96(ForecastModel):

    """
    Lorenz '96 system with locations around a circle

    Parameters
    ----------
    cfg : dict
        model configuration

    """

    kind = 'Lorenz96'
    dimensions = []
    variables = [Variable('state', 'd', (), '-')]

    def __init__(self, cfg):
        self._solver = integrate.ode(self._rhs).set_integrator(cfg['solver'],
                                                               atol=1e-12, rtol=1e-10,
                                                               )
        self._solver.set_f_params(cfg['size'], cfg['forcing'])

    ### pickle (required for paralle processing with pp) cannot handle static methods
    # @staticmethod
    def _rhs(self, t, x, size, forcing):
        new_x = np.empty_like(x)
        for i in range(size):
            new_x[i] = (x[(i+1) % size] - x[i-2]) * x[i-1] - x[i] + forcing
        return new_x

    def init_model(self, cfg):
        self.init_locations(cfg['size'])
        self.state = np.full(cfg['size'], 0.5)
        self.state[0] = 0.6

    @classmethod
    def init_ensemble(cls, cfg, ensmbl_size):
        cls.init_locations(cfg['size'])
        ensmbl = np.full((cfg['size'], ensmbl_size), 0.5)
        ensmbl[0] = np.random.normal(0.5, 0.5, ensmbl_size)

        model_instances = []
        for member in ensmbl.T:
            model_instances.append(cls(cfg))
            model_instances[-1].state = member

        return model_instances

    def forecast(self, t, dt):
        self._solver.set_initial_value(self.state, 0)

        try:
            self.state = self._solver.integrate(dt.total_seconds())
        except:
            raise Exception("Error in ODE integration at time {0}".format(t))

    @classmethod
    def init_locations(cls, size):
        cls.filter_locations = PeriodicIndexLocation(range(size), totalsize=size)

    def get_filter_state(self, index):
        return self.state[index]

    def update_state(self, new_filter_state, index):
        self.state[index] = new_filter_state

    def deconstruct(self):
        return {'state': self.state}
