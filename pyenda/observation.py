#!/usr/bin/env python
# -*- coding: utf-8 -*-

# authors:
# * Merker, Claire <claire.merker@uni-hamburg.de>
# * Geppert, Gernot <gernot.geppert@uni-hamburg.de>


"""
Observations
------------

One observation file can contain several types of observations as different
netcdf varibles. The observation error covariance, however, is one variable
and spans all observations in a file. The order of observations in the
covariance matrix is determined by the construct and deconstruct methods
of the observation operator.

Version
-------
* 2015-12-08 - initial version
* 2016-01-06 - generalized read_obs_from_file

To Do
-----
* add function to Observation that returns the location(s) of elmenents of the
  observation vector; the returned location should consist of a coordinate
  system (cartopy.ccrs.CRS or 1D/2D/3D cartesian or None for single
  points/elements)
* add reading of location information from file to read_obs_from_file
* add location information to forecast model, consisting of coordinate system;
  essentially this should work as the function for the observation vector in
  that it returns the location(s) of elements of the state vector; for writing
  data to file, there could be a seperate deconstruct method that creates
  coordinate fields (1D lat/lon or 1D rlat/rlon or 2D lat/lon fields) which
  correspond to the _dimensions of the forecast model and are written to file
  as variables
* add functions to transform between coordinate systems, for geographic ones
  use transform_points methods from cartopy.crs.CRS which includes z coordinate
* use central_angle method to calculate distance between points in geographic
  coordinate systems and use Euclidean distance in cartesian coordinate systems
"""

import numpy as np
import netCDF4 as nc
from .file_writer import datetime_to_unixtime
from . import location


def _parse_slice(slice_str):
    """
    Returns a slice object or a 1D ndarray suitable for indexing from a string.
    """
    if ':' in slice_str:
        slice_list = []
        for i in slice_str.split(':'):
            try:
                slice_list.append(np.float64(i))
            except ValueError:
                slice_list.append(None)
        if len(slice_list) < 3:
            slice_list.append(None)
        return slice(*slice_list)
    else:  # assume slice_str is a list of integer indices
        return np.array([np.int64(i) for i in slice_str], dtype=np.int64)


def _make_int_slice(s):
    """
    Turn all slice components into integers suitable for indexing arrays.
    """
    try:
        if s.start is not None:
            start = np.int64(s.start)
        else:
            start = None
        if s.stop is not None:
            stop = np.int64(s.stop)
        else:
            stop = None
        if s.step is not None:
            step = np.int64(s.step)
        else:
            step = None
        return slice(start, stop, step)
    except AttributeError:  # it's an ndarray for indexing
        return s


def _fill_slice(s, array):
    """
    Fill None values for start and stop of the slice with the first and the
    last element of the array.
    """
    if s.start is None:
        start = array.min()
    else:
        start = s.start
    if s.stop is None:
        stop = array.max()
    else:
        stop = s.stop
    ### avoid empty slices due to start==stop
    if start == stop:
        stop += 1e-12
    return slice(start, stop, s.step)


def _expand_coordinates(fid, varname, coord_names):
    """
    Returns a list whose entries are ndarrays that match the shape of the
    variable varname and hold the values for the respective coordinates, eg.
    for a variable with the shape (2, 2) definded on a 2x2 lat/lon grid,
    this could be
    [ [[1, 1], [2, 2]],
      [[1, 2], [1, 2]] ].

    coord_names are assumed to be in order of dimensions
    """

    coords = {}
    for coord in coord_names:
        coords[coord] = {}
        try:
            coords[coord]['values'] = fid[coord][:]  # np.atleast_1d(fid[coord][:])
            coords[coord]['dims'] = fid[coord].dimensions

        ### if coord is not a variable, generate values according to dim length
        ### and if coord is not in variable dimensions, raise an exception
        except:
            coord_dim = coord.lower()
            try:
                coord_index = fid[varname].dimensions.index(coord_dim)
                coords[coord]['values'] = np.arange(fid[varname].shape[coord_index])
                coords[coord]['dims'] = (coord_dim,)
                print ("Values for coordinate {0} not found, setting them to "
                       "{1}...{2}.").format(coord,
                                            coords[coord]['values'][0],
                                            coords[coord]['values'][-1])
            except ValueError:
                ### for 1D (time,) variable, set coord values to scalar 0
                ### since there is only one point
                if fid[varname].ndim == 1:
                    coords[coord]['values'] = np.array(0, dtype=np.float64)
                    coords[coord]['dims'] = (coord_dim,)
                    print ("Values for coordinate {0} not found, setting them to "
                           "{1}.").format(coord,
                                          coords[coord]['values'])
                else:
                    raise Exception(("Coordinate {0} not found in {1}'s dimensions. "
                                     "(Note that coordinates are cast to lower case "
                                     "for comparison with variable dimensions.)").format(
                                         coord, varname)
                                    )

    coord_dims = coords[coord_names[0]]['dims']

    ### if the variable has only two dimensions (ie. 'time' and some location),
    ### we do not need to expand the coordinates since they will be 1D arrays
    ### already (this does not cover mixed cases like an array for lons and one
    ### lat value for all points)
    if fid[varname].ndim < 3:
        all_coords_are_dims = False
    else:
        all_coords_are_dims = ((len(coord_dims) == 1) and
                               coord_dims[0] in fid[varname].dimensions
                               )

        for coord in coord_names[1:]:
            coord_dims = coords[coord]['dims']
            coord_is_dim = ((len(coord_dims) == 1) and
                            coord_dims[0] in fid[varname].dimensions
                            )
            if not coord_is_dim == all_coords_are_dims:
                raise Exception(("Cannot handle coordinates which are mixed "
                                 "dimensions and full fields for variable {0}."
                                 ).format(varname)
                                )

    if all_coords_are_dims:
        arrays = []
        for coord in coord_names:
            arrays.append(coords[coord]['values'])
        ### expand coordinates to shape of variable field
        arrays = np.meshgrid(*arrays, indexing='ij')
        for i, coord in enumerate(coord_names):
            coords[coord] = arrays[i]

    else:  # all coords are fields
        for coord in coord_names:
            coords[coord] = coords[coord]['values']
            ### coordinate must have the same shape as the variable or
            ### variable must not have location dimension and coordinate
            ### must have only one element
            if ((not coords[coord].shape == fid[varname].shape[1:]) and
                (not (fid[varname].ndim == 1 and coords[coord].shape == (1,)))
                ):
                raise Exception(("Coordinate {0}'s shape does not match "
                                 "{1}'s shape.").format(coord, varname)
                                )

    return [coords[coord].tolist() for coord in coord_names]


def select_obs_from_file(filename, varname, coord_names, slice_type, slices,
                         max_offset, loc_type, loc_type_args, state_index=None):
    try:
        fid = nc.Dataset(filename)
    except IOError:
        print('cannot open observation file ' + filename)
        raise

    ind, locations = _select_obs_from_file(fid, varname, coord_names,
                                           slice_type, slices, max_offset,
                                           loc_type, loc_type_args,
                                           state_index
                                           )
    fid.close()

    return ind, locations


def _select_obs_from_file(fid, varname, coord_names, slice_type, slices, max_offset,
                          loc_type, loc_type_args, state_index):
    """
    Returns a list of slice objects suitable for indexing the data field
    in a netCDF file and the matching locations for the assimilation

    The returned slices, used to index the data field, will yield the
    required observations.

    Assumes variable always has a time dim as first dim!

    Parameters
    ----------
    fid : netCDF4.Dataset object
        file id
    varname : string
        variable name
    coord_names : List[str]
        coordinate names
    slice_type : str
        'index' or 'latlon'
    slices : List[str]
        slices defining selected data in array
    max_offset : float
        maximum allowed distance between given slice location and selected
        observation in file (observation selection is nearest neighbour)
    loc_type : str
        location type of locations returned (see pyenda.location module)
    loc_type_args : dict
        dictionary containing optional arguments needed to instanciate some
        Location classes
    state_index : np.ndarray
        1d array - only needed if out_loc_type is IndexLocation, then this
        is the list of state vector indices

    Returns
    -------
    List[int] or List[slice]
        used to index the data field and return the required observation data
    out_locations : Location object
        locations of required observation data
    """
    ### single point with no location dimension (only time dim)
    if fid[varname].ndim == 1:
        return None, None

    loc_class = getattr(location, loc_type)
    ### if all slices elements are ':' select everything, return slice for each
    ### dim (in this case slice_type does not matter)
    try:
        if set(slices) == set(':'):
            coordinates = _expand_coordinates(fid, varname, coord_names)
            file_locations = loc_class(*coordinates, **loc_type_args)
            ### if all slices elements are ':' return all data, corresponding
            ### indexing slice is the slice selecting all elements
            return [slice(None, None, None) for s in slices], file_locations
    except TypeError:
        pass

    ### if all slices elements are not ':', select parts of the data corresponding
    ### to indicated slices/indices
    ### create 2d observation file coordinates
    coordinates = _expand_coordinates(fid, varname, coord_names)
    ### store observation file coordinates in Location object
    file_locations = loc_class(*coordinates, **loc_type_args)

    ### index slice information
    if slice_type == 'index':
        if fid[varname].ndim == 1:
            file_ind = None
        else:
            file_ind = tuple([_make_int_slice(_parse_slice(s)) for s in slices])
        if loc_type not in ['IndexLocation', 'NoLocation']:  # need location info for obs
            out_locations = file_locations[file_ind]
    ### latlon slice information
    if slice_type == 'latlon':
        ### loop through location dimensions and get slice object
        ### (start, stop, step) for each, make 1d array from slices for
        ### meshgrid call
        required_slices = []
        for dim in range(file_locations.ndim):
            sl = _fill_slice(_parse_slice(slices[dim]),
                             file_locations.array[...,dim])
            required_slices.append(np.arange(sl.start, sl.stop, sl.step))
        required_locations = type(file_locations)(*np.meshgrid(*required_slices,
                                                               indexing='ij'),
                                                  **loc_type_args)
        ### if input is a 3D field ('time', 'lat', 'lon'), return a 2D field
        ### ('lat', 'lon'), if input is 2D ('time', 'points'), return
        ### 1D field ('points')
        if fid[varname].ndim == 2:
            required_shape = (np.array(required_locations.shape).prod(),)
            required_locations = required_locations.reshape(required_shape)
        # else:
        #     required_shape = required_locations.shape
        #     required_locations = required_locations.reshape(required_shape)

        file_ind = required_locations.get_closest_indices(file_locations)
        if loc_type not in ['IndexLocation', 'NoLocation']:  # need location info for obs
            out_locations = file_locations[file_ind]
            ### check if chosen data is close enough to required_locations
            ### to be considered
            out_locations = out_locations[np.where(
                required_locations.calc_distance(out_locations)<max_offset)]
            file_ind = out_locations.get_closest_indices(file_locations)
    ### observation without location information
    if loc_type == 'NoLocation':
        out_locations = loc_class(file_ind[0].shape, **loc_type_args)
    ### observation with index location information
    if loc_type == 'IndexLocation':
        if type(state_index) is str:  # have a variable with the state indices
            state_index = fid[state_index][file_ind]
        out_locations = loc_class(state_index, **loc_type_args)

    return file_ind, out_locations


####### REWRITE vvvvvvvvvvvvvvvvvvvv #####################
### NOT IN USE
### separate into
### 1. picking obs from file
### 2. if necessary picking locations from file_locations
### 3. creating locations for assimilation
def _read_location(fid, varname, loc_type, *loc_info):
    """
    Shape in which locations and indices are returned is not guaranteed.
    Eg., for 2D ('time', 'points') data arrays, the locations will be
    returned as 2D ('lat', 'lon') arrays when value slices are required.

    !!!REWRITE!!!

    """

    ### do not associate any location type with the observation
    if loc_type == 'NoLocation':
        ### parse loc_info into integer index values
        ind = []
        for info in loc_info:
            ind.append(_parse_slice(info))
        ### if there is no loc_info, select all with slice(None)
        if len(ind) == 0:
            ind = [slice(None)]
        return NoLocation(fid['varname'][ind].shape), ind

    # if loc_type == 'IndexLocation':
    #     ### parse loc_info into integer index values
    #     ind = []
    #     for info in loc_info:
    #         ind.append(_parse_slice(info))
    #     ### if there is no loc_info, select all with slice(None)
    #     if len(ind) == 0:
    #         ind = [slice(None)]
    #     return IndexLocation(

    if loc_type == 'Geogr2DLocation':
        lat_name, lon_name = loc_info[:2]

        ### check if coordinate names are dimensions of the variable, ie.
        ### we need to expand the coordinate variables from 1D fields to
        ### fields matching the variable
        coordinates = _expand_coordinates(fid, varname, [lat_name, lon_name])
        if loc_info[2] == 'index':
            if fid[varname].ndim == 1:  # single point with no location dimension
                coord_slice = None
            elif fid[varname].ndim == 2:  # time and points
                coord_slice = _make_int_slice(_parse_slice(loc_info[3]))
            elif fid[varname].ndim == 3:  # time and lat and lon
                coord_slice = [_make_int_slice(_parse_slice(loc_info[3])),
                               _make_int_slice(_parse_slice(loc_info[4]))]
            else:
                raise Exception(("Variable with more than 3 dimensions. "
                                 "Can only handle up to 3 dimensions in "
                                 "Observation._read_location. {0}").format(
                                     varname)
                                )

            return (Geogr2DLocation(coordinates[lat_name][coord_slice],
                                   coordinates[lon_name][coord_slice]),
                    coord_slice)

        else:  # slice in latlon
            ### everything should be read, do not generate required locastions
            ### for nearest neighbour search but read everything
            if loc_info[3] == ':' and loc_info[4] == ':':
                if fid[varname].ndim == 1:  # single point with no location dimension
                    coord_slice = None
                else:
                    coord_slice = slice(None, None, None)

                return (Geogr2DLocation(coordinates[lat_name][coord_slice],
                                        coordinates[lon_name][coord_slice]),
                        coord_slice)

            ### if latlon slice is required, generate the required latlon fields
            ### from the given slice, if start or stop are omitted, use the
            ### min and max values from the fields in the file
            file_locations = Geogr2DLocation(coordinates[lat_name], coordinates[lon_name])
            file_shape = file_locations.shape
            required_lat_slice = _fill_slice(_parse_slice(loc_info[3]),
                                             file_locations.lat)
            required_lon_slice = _fill_slice(_parse_slice(loc_info[4]),
                                             file_locations.lon)

            required_locations = Geogr2DLocation(*np.meshgrid(
                                        np.arange(required_lat_slice.start,
                                                  required_lat_slice.stop,
                                                  required_lat_slice.step
                                                  ),
                                        np.arange(required_lon_slice.start,
                                                  required_lon_slice.stop,
                                                  required_lon_slice.step
                                                  ),
                                        indexing='ij'
                                                               )
                                                 )
            #import pdb; pdb.set_trace()

            ### if input is 1D ('time'), return only one location
            if fid[varname].ndim == 1:  # single point with no location dimension
                ind = None
                out = file_locations

            else:
                ### if input is a 3D field ('time', 'lat', 'lon'), return a 2D field
                ### ('lat', 'lon'), if input is 2D ('time', 'points'), return
                ### 1D field ('points')
                if fid[varname].ndim == 2:
                    required_shape = (np.array(required_locations.shape).prod(),)
                    required_locations = required_locations.reshape(required_shape)
                else:
                    required_shape = required_locations.shape
                    # required_locations = required_locations.reshape(required_shape)

                ### find index slice of file to read (use nearest neighbours)
                no_of_locations = np.asarray(required_shape).prod()
                required_locations = required_locations.reshape(no_of_locations)
                nearest_locations = np.empty(required_locations.shape + (2,))
                ### the indices returned match the shape of the required locations
                ### each entry has the same number of dimensions as the data field
                ### of the file
                nearest_indices = np.empty(required_locations.shape, dtype=np.int64)

                for i, loc in enumerate(required_locations.array):
                    min_loc = np.argmin(Geogr2DLocation.from_array(loc).calc_distance(file_locations))
                    #min_loc = np.unravel_index(min_loc, file_shape)
                    nearest_indices[i] = min_loc
                    nearest_locations[i] = file_locations.array[np.unravel_index(min_loc, file_shape)]

                out = Geogr2DLocation.from_array(nearest_locations).reshape(required_shape)
                ind = np.unravel_index(nearest_indices.reshape(required_shape),
                                       file_shape)

            return out, ind
####### REWRITE ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ #####################


class Observation(object):
    """store observation, covariance matrix, time and location

    Attributes
    ----------
    _time : Datetime object
        datetime.datetime
    _location : List[float]
        list of floats containing locations
    _operator : ObsOperator class
        ObsOperator class to use for observation
    _vector : np.ndarray
        1d array containing observation
    _covariance : np.ndarray or float
        2d array with observation covariance matrix  or
        float with observation variance (diag. covariance matrix)

    Parameters
    ----------
    time : Datetime object
        datetime.datetime
    operator : ObsOperator class
        ObsOperator class to use for observation
    locations : Optional[List[float]]
        list of floats containing locations
    vector : Optional[np.ndarray]
        1d array containing observation
    covariance : Optional[np.ndarray or float]
        2d array with observation covariance matrix  or
        float with observation standard deviation (diag. covriance matrix)
    """
    def __init__(self, time, operator, locations=None, vector=None,
                 covariance=None, domains=None):
        """constructor"""
        self._time = time
        self._locations = locations
        self._operator = operator
        self._vector = vector
        self._covariance = covariance
        self._domains = domains

    @property
    def vector(self):
        return self._vector

    @property
    def covariance(self):
        return self._covariance

    @property
    def time(self):
        return self._time

    @property
    def locations(self):
        return self._locations

    @property
    def operator(self):
        return self._operator

    @property
    def domains(self):
        return self._domains

    def read_obs_from_file(self, t, file_name_obs, file_ind, locations, file_name_cov):
        """
        Read observation from file.
        Time closest to t will be used

        Parameters
        ----------
        t : Datetime object
            datetime.datetime, time of observation to select from file
        file_name_obs : str
            path and name of observation data file
        file_ind : Tuple[np.ndarray]
            tuple with indexing arrays
        locations : List[float]
            list of floats containing locations
        file_name_cov : str or float
            path and name of observation covariance file or float indicating
            constant variance
        """
        try:
            fid_obs = nc.Dataset(file_name_obs, 'r')
        except IOError:
            print('cannot open observation file ' + file_name_obs)
            raise

        time = datetime_to_unixtime(t)
        time_ind = np.argmin(abs(fid_obs['time'][:]-time))

        obs = {}
        for var in self.operator.variables:
            try:
                ### squeeze out extra time dimension from indexing with ind
                obs[var.name] = fid_obs[var.name][time_ind].squeeze()[file_ind]
            except:
                raise Exception(("Observation at {0} ({1:f}) in {2} not "
                                 "found.\n "
                                 "ind: {3}").format(t, time, file_name_obs, file_ind)
                                )
        fid_obs.close()

        self._vector, _ = self.operator.construct(obs, locations)

        ### if file_name_cov is a string, read covariance from file,
        ### otherwise assume file_name_cov is a float and use as covariance
        ### value
        try:
            fid_cov = nc.Dataset(file_name_cov, 'r')
            if 'time' in fid_cov.variables['obs_covariance'].dimensions:
                ind = np.where(fid_cov['time'][:]==time)
                try:
                    covar = fid_cov['obs_covariance'][ind].squeeze()
                except ValueError:
                    raise Exception(("obs_covariance at {0} ({1:f}) in {2} not "
                                     "found.".format(t, time, file_name_cov)))
            else:
                covar = fid_cov.variables['obs_covariance'][:]
                fid_cov.close()
        except IOError:
            covar = np.atleast_1d(file_name_cov)

        ### if covar is not a matrix: make it diagonal matrix
        if covar.ndim < 2:
            if covar.size == 1:
                covar = np.repeat(covar, self.vector.shape[0])
            covar = np.diagflat(covar)

        if (covar.shape[0] != self.vector.shape[0]) or (covar.shape[0] != covar.shape[1]):
            raise Exception(("Observation error covariance matrix size does not "
                             "match length of observation vector."))

        self._covariance = covar

    ### ! not used >>>
    def _read_obs_from_file(self, t, file_name_obs, file_name_cov,
                            slice_type, slices, out_loc_type, state_index=None
                            ):
        """
        Read observation from file.

        Parameters
        ----------
        t : Datetime object
            datetime.datetime
        file_name_obs : str
            path and name of observation data file
        file_name_cov : str
            path and name of observation covarianc file
        slice_type : str
            'index' or 'latlon'
        slices : List[str]
            slices defining selected data in array
        out_loc_type : str
            location type of locations returned (see pyenda.location module)
        state_index : Optional[np.ndarray]
            1d array - only needed if out_loc_type is IndexLocation, then this
            is the list of state vector indices
        """

        fid_obs = nc.Dataset(file_name_obs, 'r')
        time = datetime_to_unixtime(t)
        time_ind = np.where(fid_obs['time'][:]==time)
        obs, loc = {}, {}
        for var in self._operator.variables:
            try:
                ind, loc[var.name] = _select_obs_from_file(fid_obs, var.name,
                                                           slice_type, slices,
                                                           out_loc_type, state_index
                                                           )
                ### squeeze out extra time dimension from indexing with ind
                obs[var.name] = fid_obs[var.name][time_ind][ind].squeeze()

            except ValueError:
                raise Exception(("Observation at {0} ({1:f}) in {2} not "
                                 "found.\n "
                                 "ind: {3}").format(t, time, file_name_obs, ind)
                                )
            ### old docstring for *loc_info:
            # loc_info : variable number of str
            #     var names or dim names of coordinates (if dim name, values will be created),
            #     'index' or 'value',
            #     slices for coordinates;
            #     eg. "Lat, Lon, index, :, :"
            #         - read values from variables 'Lat' and 'Lon', use all data points,
            #         "Lat, Lon, value, 10:20:2, 0:50:5"
            #         - read values from variables 'Lat' and 'Lon', use data points
            #           nearest to grid points of 10:20:2 by 0:50:5,
            #         "lat, lon, index, 2:4, 1:10"
            #         - coordinate values will be 0..len(lat dim) and 0..len(lon dim),
            #           read data points at indices 2..4 of lat dim and 1..10 of lon dim
            # try:
            #     loc[var.name], location_ind = _read_location(fid_obs, var.name, loc_type, *loc_info)
            #     ### squeeze out extra time dimension from indexing with ind
            #     obs[var.name] = fid_obs[var.name][time_ind][location_ind].squeeze()

            # except ValueError:
            #     raise Exception(("Observation at {0} ({1:f}) in {2} not "
            #                      "found.\n "
            #                      "loc_info: {3}").format(t, time, file_name_obs, loc_info)
            #                     )
        fid_obs.close()
        ### do we need to pass locations to construct?
        ### values of obs dict and location should have the same shape, so pass
        ### obs dict and location to construct and return matching vectors of
        ### observations and locations; these vectors will be used in F
        #...
        self._vector, self._location = self._operator.construct(obs, loc)

        fid_cov = nc.Dataset(file_name_cov, 'r')
        if 'time' in fid_cov.variables['obs_covariance'].dimensions:
            ind = np.where(fid_cov['time'][:]==time)
            try:
                covar = fid_cov['obs_covariance'][ind]
            except ValueError:
                raise Exception(("obs_covariance at {0} ({1:f}) in {2} not "
                                 "found.".format(t, time, file_name_obs)
                                 )
                                )
        else:
            covar = fid_cov.variables['obs_covariance'][:]
        fid_cov.close()

        if covar.ndim < 2:
            if covar.ndim == 0:
                covar = np.repeat(covar, self.size)
            covar = np.diagflat(covar)

        self._covariance = covar
