#!/usr/bin/env python
# -*- coding: utf-8 -*-

# authors:
# * Geppert, Gernot <gernot.geppert@uni-hamburg.de>


"""
Location Information
--------------------

contains location classes

Note
----

convention for polar coordinates:
    (r, phi) corresponds to (radius, azimuth)

convention for spherical coordinates:
    (r, theta, phi) corresponds to (radius, azimuth, inclination)

Version
-------
* 2016-01-07 - initial version

To Do
-----
* comment and clean up!
"""

from abc import ABCMeta, abstractmethod, abstractproperty
import numpy as np
import logging
from copy import copy

deg2rad = np.pi/180
rad2deg = 180/np.pi

deg2rad = np.pi/180
rad2deg = 180/np.pi


def _check_shape(*arrays):
    shape = arrays[0].shape
    for array in arrays[1:]:
        if not shape == np.asarray(array).shape:
            raise Exception("All arrays need to have the same shape.")


def _check_equal_type(loc1, loc2):
    if type(loc1) is type(loc2):
        return True
    else:
        return False


class Location(object):
    """
    Abstract base class for locations.
    """

    __metaclass__ = ABCMeta

    def __init__(self, **loc_kw_args):
        self.coord_dims = loc_kw_args.pop('coord_dim_names', None)

    def calc_distance(self, loc):
        """
        Abstract method to calculate distance to other location.
        """
        if _check_equal_type(self, loc):
            return self._calc_distance(loc)
        else:
            raise Exception(("Locations do not have the same type, "
                             "cannot calculate distance ({0}, {1})."
                             ).format(type(self), type(loc))
                            )

    @abstractmethod
    def _calc_distance(self, loc):
        """
        Abstract method to calculate distance to other location.
        """

    @abstractproperty
    def array(self):
        """
        Abstract method to return location components combined in array.
        """

    @abstractmethod
    def reshape(self, shape, coord_dim_names, order):
        """
        Abstract method to reshape the attribute arrays.

        The abstract method takes care of changing coordinate dimension names
        when reshaping and has to be called within the concrete reshape method
        of a child class.
        """

        self.coord_dims = coord_dim_names

    @property
    def shape(self):
        return self.array[..., 0].shape

    @property
    def size(self):
        return self.array[..., 0].size

    @property
    def ndim(self):
        return self.array[..., 0].ndim

    @classmethod
    @abstractmethod
    def from_array(cls, array):
        """
        Create location from ndarray where the location components are
        in the last dimension.

        """

    ### class attribute instead of property!
    # @abstractproperty
    # def var_names(self):
    #     """
    #     Abstract property that returns the variable names of the location
    #     coordinates for writing them to a netCDF file, eg. ['Latitude',
    #     Longitude'] for a geographic 2D location.

    #     """

    @property
    def coord_dims(self):
        """
        Return list of netCDF dimension tuples.

        """

        return list(zip(self._dim_names, self.shape))

    @coord_dims.setter
    def coord_dims(self, names):
        """
        Set coord dimension names.

        If the locations array is 1D, the dimension name will be 'points',
        otherwise the dimension names will be the lower case variable names.

        Parameters
        ----------
        names : Optional[List[str]]
            If given, will be used for the dimension names.

        """

        if names is not None:
            self._dim_names = names
        else:
            if self.ndim == 1:
                self._dim_names = ['points']
            else:
                self._dim_names = [s.lower() for s in self.var_names]

        if len(self._dim_names) != len(self.shape):
            raise Exception(("The dimensions names do not match the dimensions "
                             "of the locations\n"
                             "{0}\n"
                             "{1}".format(self._dim_names, self.shape)
                             )
                            )

    def __getitem__(self, key):
        return self.from_array(self.array[key])

    def __iter__(self):
        ### reshape to 1D location array and iterate through
        for loc_array in self.array.reshape((-1, self.array.shape[-1])):
            yield self.from_array(loc_array)

    def get_closest_indices(self, x):
        """
        Returns a tuple of ndarrays suitable for indexing x (similar to
        numpy.unravel_index). The arrays will have the shape
        of self and should be used to index x like x[arr1, arr2,
        ... arrn] when x.ndim == n.

        It is easier to use the result like x[list(ind)]

        """

        min_locations = np.full(self.shape + (x.ndim,), np.nan, dtype=np.int64)
        for i, loc in enumerate(self):
            min_loc_x = np.unravel_index(np.argmin(loc.calc_distance(x)), x.shape)

            min_locations[np.unravel_index(i, self.shape)] = min_loc_x

        # min_locations = np.full((np.asarray(self.shape).prod(), x.ndim), np.nan)
        # for i, loc in enumerate(self):
        #     min_locations[i] = np.unravel_index(np.argmin(loc.calc_distance(x)), x.shape)

        ### had to do it like because otherwise the indexing in the loop
        ### would have been complicated and ugly, now min_locations - before return -
        ### is like an array of the shape of self which holds multi-dimensional indices
        ### into x, then rolling the last axis to the front yields an array as described
        ### in the doc
        return tuple(np.rollaxis(min_locations, -1, 0))

    # @abstractmethod
    # def write_to_file(self, writer):
    #     """
    #     Abstract method to write locations to Writer instance.

    #     """


### these functions work with ndarrays

def _euclidean_distance(x, y):
    x = np.asarray(x, dtype=np.float64)
    y = np.asarray(y, dtype=np.float64)
    return np.sqrt(((x-y)**2).sum(axis=-1))


def _cartesian_to_polar(x):
    x = np.asarray(x, dtype=np.float64)
    out = np.empty_like(x)
    out[..., 0] = _euclidean_distance(x, np.array([0, 0]))
    out[..., 1] = np.arctan2(x[..., 1], x[..., 0])
    return out


def _cartesian_to_spherical(x):
    x = np.asarray(x, dtype=np.float64)
    out = np.empty_like(x)
    out[..., 0] = _euclidean_distance(x, [0, 0, 0])
    out[..., 1] = np.arctan2(x[..., 1], x[..., 0])
    ind = np.where(out[..., 0]!=0)
    out[..., 2] = 0
    out[..., 2][ind] = np.arccos(x[..., 2][ind] / out[..., 0][ind])
    return out


def _polar_to_cartesian(x):
    x = np.asarray(x, dtype=np.float64)
    out = np.empty_like(x)
    out[..., 0] = x[..., 0] * np.cos(x[..., 1])
    out[..., 1] = x[..., 0] * np.sin(x[..., 1])
    return out


def _spherical_to_cartesian(x):
    x = np.asarray(x, dtype=np.float64)
    out = np.empty_like(x)
    out[..., 0] = x[..., 0] * np.cos(x[..., 1]) * np.sin(x[..., 2])
    out[..., 1] = x[..., 0] * np.sin(x[..., 1]) * np.sin(x[..., 2])
    out[..., 2] = x[..., 0] * np.cos(x[..., 2])
    return out


def _central_angle(latlon1, latlon2):
    latlon1 = np.asarray(latlon1, dtype=np.float64)
    latlon2 = np.asarray(latlon2, dtype=np.float64)
    return 2 * np.arcsin(np.sqrt(np.sin((latlon1[..., 0]-latlon2[..., 0])/2.)**2 +
                                 np.cos(latlon1[..., 0]) * np.cos(latlon2[..., 0]) *
                                 np.sin((latlon1[..., 1]-latlon2[..., 1])/2.)**2
                                 )
                         )


class NoLocation(Location):
    """
    Class for data without location information.

    Attributes
    ----------
    _shape : List[int]
        list indicating the shape of the data for which there are no
        locations

    Parameters
    ----------
    shape : List[int]
        list indicating the shape of the data for which there are no
        locations
    """

    var_names = []

    def __init__(self, shape, **loc_kw_args):
        """constructor"""
        self._shape = tuple(shape)
        super(NoLocation, self).__init__(**loc_kw_args)

    @property
    def array(self):
        return np.array([])

    ### for automatic creation of dim_names for output
    @property
    def ndim(self):
        return len(self.shape)

    @property
    def shape(self):
        return self._shape

    # @property
    # def var_names(self):
    #     return []

    def reshape(self, shape, coord_dim_names=None, order='C'):
        out = copy(self)
        out._shape = shape
        super(NoLocation, out).reshape(shape, coord_dim_names, order)
        return out

    @staticmethod
    def _calc_distance(x):
        return None


class IndexLocation(Location):
    """class for (observation) data that use the index of the filter state
    vector to associate a location with an observation, eg. an index location
    of 10 corresponds to the 10th element of the filter state vector.

    Attributes
    ----------
    _index : List[int]
        index for each dimension

    Parameters
    ----------
    index : List[int]
        index for each dimension
    **kwargs
        Arbitrary keyword arguments

    Class Attributes
    ----------------
    var_names : List[str]
        location dimension names
    """

    var_names = ['state_index']

    def __init__(self, index, **loc_kw_args):
        index = np.atleast_1d(np.asarray(index, dtype=np.int64))
        _check_shape(index)
        self._index = index
        super(IndexLocation, self).__init__(**loc_kw_args)

    @property
    def index(self):
        return self._index

    @property
    def array(self):
        ### return array of arrays with one element, the index
        return np.expand_dims(self.index, -1)

    # @property
    # def var_names(self):
    #     return ['state_index']

    def reshape(self, shape, coord_dim_names=None, order='C'):
        out = copy(self)
        out._index = out._index.reshape(shape, order=order)
        super(IndexLocation, out).reshape(shape, coord_dim_names, order)
        return out

    def _calc_distance(self, x):
        return abs(self.array - x.array)

    @Location.coord_dims.setter
    def coord_dims(self, names):
        self._dim_names = ['state_index']

    ### Should probably be removed because if IndexLocation has distances, then there are also
    ### closest indices
    # def get_closest_indices(self, x):
    #     return (self.index,)

    @classmethod
    def from_array(cls, a):
        """
        Construct Location from array with coordinates in the last dimension.
        """
        array = np.asarray(a)
        return cls(a)


class Cartesian1DLocation(Location):
    """
    Attributes
    ----------
    _x : float
        cartesian location 1st dimension

    Parameters
    ----------
    x : float
        cartesian location 1st dimension
    **kwargs
        Arbitrary keyword arguments.

    Class Attributes
    ----------------
    var_names : List[str]
        location dimension names
    """
    var_names = ['x']

    def __init__(self, x, **loc_kw_args):
        x = np.atleast_1d(np.asarray(x, dtype=np.float64))
        _check_shape(x)
        self._x = x
        super(Cartesian1DLocation, self).__init__(**loc_kw_args)

    @property
    def x(self):
        return self._x

    # def write_to_file(self, writer, dims):
    #     writer._fid.createVariable(Dimname, 'd', (dimname,))
    #     ...

    @property
    def array(self):
        ### return array of arrays with one element, the index
        return np.expand_dims(self.x, -1)

    def reshape(self, shape, coord_dim_names=None, order='C'):
        out = copy(self)
        out._x = out._x.reshape(shape, order=order)
        super(Cartesian1DLocation, out).reshape(shape, coord_dim_names, order)
        return out

    def _calc_distance(self, x):
        return abs(self.array-x.array)

    @classmethod
    def from_array(cls, a):
        """
        Construct Location from array with coordinates in the last dimension.
        """
        array = np.asarray(a)
        return cls(a)

    @Location.coord_dims.setter
    def coord_dims(self, names):
        self._dim_names = ['x']


class Cartesian2DLocation(Location):
    """
    Attributes
    ----------
    _x : float
        cartesian location 1st dimension
    _y : float
        cartesian location 2nd dimension

    Parameters
    ----------
    x : float
        cartesian location 1st dimension
    y : float
        cartesian location 2nd dimension
    **kwargs
        Arbitrary keyword arguments.

    Class Attributes
    ----------------
    var_names : List[str]
        location dimension names
    """
    var_names = ['x', 'y']

    def __init__(self, x, y, **loc_kw_args):
        x = np.atleast_1d(np.asarray(x, dtype=np.float64))
        y = np.atleast_1d(np.asarray(y, dtype=np.float64))
        _check_shape(x, y)
        self._x = x
        self._y = y
        super(Cartesian2DLocation, self).__init__(**loc_kw_args)

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    @property
    def array(self):
        return np.stack((self.x, self.y), axis=-1)

    def reshape(self, shape, coord_dim_names=None, order='C'):
        out = copy(self)
        out._x = out._x.reshape(shape, order=order)
        out._y = out._y.reshape(shape, order=order)
        super(Cartesian2DLocation, out).reshape(shape, coord_dim_names, order)
        return out

    def _calc_distance(self, x):
        return _euclidean_distance(self.array, x.array)

    @classmethod
    def from_array(cls, a):
        """
        Construct Location from array with coordinates in the last dimension.

        """

        array = np.asarray(a)
        return cls(array[..., 0], array[..., 1])

    def transform_points(self, target_cs, **kwargs):
        if target_cs is PolarLocation:
            return PolarLocation.from_array(_cartesian_to_polar(self.array))
        elif target_cs is SphericalLocation:
            loc3d = self.transform_points(self, Cartesian3DLocation, **kwargs)
            return SphericalLocation.from_array(_cartesian_to_spherical(loc3d.array))
        elif target_cs is Cartesian3DLocation:
            z = kwargs.pop('z', 0)
            z = np.full((self.array.shape[:-1])+(1,), z)
            return Cartesian3DLocation.from_array(np.append(self.array, z, axis=-1))


class Cartesian3DLocation(Cartesian2DLocation):
    """
    Attributes
    ----------
    _x : float
        cartesian location 1st dimension
    _y : float
        cartesian location 2nd dimension
    _z : float
        cartesian location 3rd dimension

    Parameters
    ----------
    x : float
        cartesian location 1st dimension
    y : float
        cartesian location 2nd dimension
    y : float
        cartesian location 3rd dimension
    **kwargs
        Arbitrary keyword arguments.

    Class Attributes
    ----------------
    var_names : List[str]
        location dimension names
    """
    var_names = ['x', 'y', 'z']

    def __init__(self, x, y, z, **loc_kw_args):
        x = np.atleast_1d(np.asarray(x, dtype=np.float64))
        y = np.atleast_1d(np.asarray(y, dtype=np.float64))
        z = np.atleast_1d(np.asarray(z, dtype=np.float64))
        _check_shape(x, y, z)
        self._x = x
        self._y = y
        self._z = z
        super(Cartesian3DLocation, self).__init__(**loc_kw_args)

    @property
    def z(self):
        return self._z

    @property
    def array(self):
        return np.stack((self.x, self.y, self.z), axis=-1)

    def reshape(self, shape, coord_dim_names=None, order='C'):
        out = copy(self)
        out._x = out._x.reshape(shape, order=order)
        out._y = out._y.reshape(shape, order=order)
        out._z = out._z.reshape(shape, order=order)
        super(Cartesian3DLocation, out).reshape(shape, coord_dim_names, order)
        return out

    @classmethod
    def from_array(cls, a):
        """
        Construct Location from array with coordinates in the last dimension.

        """

        array = np.asarray(a)
        return cls(array[..., 0], array[..., 1], array[..., 2])

    def transform_points(self, target_cs, **kwargs):
        if target_cs is PolarLocation:
            return PolarLocation.from_array(_cartesian_to_polar(self.array[..., :2]))
        elif target_cs is SphericalLocation:
            return SphericalLocation.from_array(_cartesian_to_spherical(self.array))
        elif target_cs is Cartesian2DLocation:
            return Cartesian2DLocation.from_array(self.array[..., :2])


class PolarLocation(Location):
    """
    Attributes
    ----------
    _r : float
        radius
    _phi : float
        azimuth

    Parameters
    ----------
    r : float
        radius
    phi : float
        azimuth
    **kwargs
        Arbitrary keyword arguments.

    Class Attributes
    ----------------
    var_names : List[str]
        location dimension names
    """
    var_names = ['r', 'phi']

    def __init__(self, r, phi, **loc_kw_args):
        r = np.atleast_1d(np.asarray(r, dtype=np.float64))
        phi = np.atleast_1d(np.asarray(phi, dtype=np.float64))
        _check_shape(r, phi)
        self._r = r
        self._phi = phi
        super(PolarLocation, self).__init__(**loc_kw_args)

    @property
    def r(self):
        return self._r

    @property
    def phi(self):
        return self._phi

    @property
    def array(self):
        return np.stack((self.r, self.phi), axis=-1)

    def reshape(self, shape, coord_dim_names=None, order='C'):
        out = copy(self)
        out._r = out._r.reshape(shape, order=order)
        out._phi = out._phi.reshape(shape, order=order)
        super(PolarLocation, out).reshape(shape, coord_dim_names, order)
        return out

    @classmethod
    def from_array(cls, a):
        """
        Construct Location from array with coordinates in the last dimension.
        """

        array = np.asarray(a)
        return cls(array[..., 0], array[..., 1])

    def _calc_distance(self, x):
        return (np.sqrt(self.r**2 + x.r**2 - 2 * self.r * x.r *
                            (np.cos(self.phi) * np.cos(x.phi) +
                             np.sin(self.phi) * np.sin(x.phi)
                             )
                        )
                )

    def transform_points(self, target_cs, **kwargs):
        if target_cs is Cartesian2DLocation:
            return Cartesian2DLocation.from_array(_polar_to_cartesian(self.array))
        elif target_cs is Cartesian3DLocation:
            loc2d = self.transform_points(Cartesian2DLocation)
            z = kwargs.pop('z', 0)
            z = np.full((self.array.shape[:-1])+(1,), z)
            return Cartesian3DLocation.from_array(np.append(loc2d.array, z, axis=-1))
        elif target_cs is SphericalLocation:
            loc3d = self.transform_points(Cartesian3DLocation, **kwargs)
            return SphericalLocation.from_array(_cartesian_to_spherical(loc3d.array))


class SphericalLocation(PolarLocation):
    """
    Attributes
    ----------
    _r : float
        radius
    _theta : float
        azimuth
    _phi : float
        inclination

    Parameters
    ----------
    r : float
        radius
    theta : float
        azimuth
    phi : float
        inclination
    **kwargs
        Arbitrary keyword arguments.

    Class Attributes
    ----------------
    var_names : List[str]
        location dimension names
    """
    var_names = ['r', 'theta', 'phi']

    def __init__(self, r, theta, phi, **loc_kw_args):
        r = np.atleast_1d(np.asarray(r, dtype=np.float64))
        theta = np.atleast_1d(np.asarray(theta, dtype=np.float64))
        phi = np.atleast_1d(np.asarray(phi, dtype=np.float64))
        _check_shape(r, theta, phi)
        self._r = r
        self._theta = theta
        self._phi = phi
        super(SphericalLocation, self).__init__(**loc_kw_args)

    ### note that phi is azimuth in PolarLocation but inclination in SphericalLocation
    @property
    def theta(self):
        return self._theta

    @property
    def array(self):
        return np.stack((self.r, self.theta, self.phi), axis=-1)

    def reshape(self, shape, coord_dim_names=None, order='C'):
        out = copy(self)
        out._r = out._r.reshape(shape, order=order)
        out._theta = out._theta.reshape(shape, order=order)
        out._phi = out._phi.reshape(shape, order=order)
        super(SphericalLocation, out).reshape(shape, coord_dim_names, order)
        return out

    @classmethod
    def from_array(cls, a):
        """
        Construct Location from array with coordinates in the last dimension.
        """

        array = np.asarray(a)
        return cls(array[..., 0], array[..., 1], array[..., 2])

    def _calc_distance(self, x):
        return (np.sqrt(self.r**2 + x.r**2 - 2 * self.r * x.r *
                            (np.sin(self.phi) * np.sin(x.phi) *
                             np.cos(self.theta - x.theta) +
                             np.cos(self.phi) * np.cos(x.phi)
                             )
                        )
                )

    def transform_points(self, target_cs):
        if target_cs is Cartesian3DLocation:
            return Cartesian3DLocation.from_array(_spherical_to_cartesian(self.array))
        elif target_cs is Cartesian2DLocation:
            loc3d = self.transform_points(Cartesian3DLocation)
            return loc3d.transform_points(Cartesian2DLocation)
        elif target_cs is PolarLocation:
            loc2d = self.transform_points(Cartesian2DLocation)
            return loc2d.transform_points(PolarLocation)


class Geogr2DLocation(Location):
    """
    Attributes
    ----------
    _lat : float
        latitude
    _lon : float
        longitude

    Parameters
    ----------
    lat : float
        latitude
    lon : float
        longitude
    r_earth : Optional[float]
        earth radius
    **kwargs
        Arbitrary keyword arguments.

    Class Attributes
    ----------------
    var_names : List[str]
        location dimension names
    """
    var_names = ['latitude', 'longitude']

    def __init__(self, lat, lon, r_earth=6371000, **loc_kw_args):
        lat = np.atleast_1d(np.asarray(lat, dtype=np.float64))
        lon = np.atleast_1d(np.asarray(lon, dtype=np.float64))
        _check_shape(lat, lon)
        self._lat = lat
        self._lon = lon
        self._r_earth = np.float64(r_earth)
        super(Geogr2DLocation, self).__init__(**loc_kw_args)

    @property
    def lat(self):
        return self._lat

    @property
    def lon(self):
        return self._lon

    @property
    def r_earth(self):
        return self._r_earth

    @property
    def array(self):
        return np.stack((self.lat, self.lon), axis=-1)

    def reshape(self, shape, coord_dim_names=None, order='C'):
        out = copy(self)
        out._lat = out._lat.reshape(shape, order=order)
        out._lon = out._lon.reshape(shape, order=order)
        super(Geogr2DLocation, out).reshape(shape, coord_dim_names, order)
        return out

    @classmethod
    def from_array(cls, a):
        """
        Construct Location from array with coordinates in the last dimension.
        """

        array = np.asarray(a)
        return cls(array[..., 0], array[..., 1])

    def _calc_distance(self, x):
        """
        Calculate great circle distance via central angle.
        """

        return _central_angle(self.array*deg2rad, x.array*deg2rad) * self.r_earth

    def transform_points(self, target_cs):
        if target_cs is SphericalLocation:
            r = np.full((self.array.shape[:-1])+(1,), self.r_earth)
            return SphericalLocation.from_array(np.append((r, self.array*deg2rad), axis=-1))
        elif target_cs is Cartesian3DLocation:
            locsph = self.transform_points(SphericalLocation)
            return Cartesian3DLocation.from_array(locsph.array)


class Geogr3DLocation(Geogr2DLocation):
    """
    Attributes
    ----------
    _lat : float
        latitude
    _lon : float
        longitude
    _height: float
        height

    Parameters
    ----------
    lat : float
        latitude
    lon : float
        longitude
    height: float
        height
    r_earth : Optional[float]
        earth radius
    **kwargs
        Arbitrary keyword arguments.

    Class Attributes
    ----------------
    var_names : List[str]
        location dimension names
    """
    var_names = ['latitude', 'longitude', 'height']

    def __init__(self, lat, lon, height, r_earth=6371000, **loc_kw_args):
        lat = np.atleast_1d(np.asarray(lat, dtype=np.float64))
        lon = np.atleast_1d(np.asarray(lon, dtype=np.float64))
        height = np.atleast_1d(np.asarray(height, dtype=np.float64))
        _check_shape(lat, lon, height)
        self._lat = lat
        self._lon = lon
        self._height = height
        self._r_earth = r_earth
        super(Geogr3DLocation, self).__init__(**loc_kw_args)

    @property
    def height(self):
        return self._height

    @property
    def array(self):
        return np.stack((self.lat, self.lon, self.height), axis=-1)

    # @property
    # def var_names(self):
    #     return ['Latitude', 'Longitude', 'Height']

    def reshape(self, shape, coord_dim_names=None, order='C'):
        out = copy(self)
        out._lat = out._lat.reshape(shape, order=order)
        out._lon = out._lon.reshape(shape, order=order)
        out._height = out._height.reshape(shape, order=order)
        super(Geogr3DLocation, out).reshape(shape, coord_dim_names, order)
        return out

    @classmethod
    def from_array(cls, a):
        """
        Construct Location from array with coordinates in the last dimension.
        """

        array = np.asarray(a)
        return cls(array[..., 0], array[..., 1], array[..., 2])

    def _calc_distance(self, x):
        """
        Calculate Euclidean distance between two points.
        """

    def transform_points(self, target_cs):
        if target_cs is SphericalLocation:
            r = np.full((self.array.shape[:-1])+(1,), self.r_earth) + self.array[..., 2]
            return SphericalLocation.from_array(np.append((r, self.array[..., :2] * deg2rad), axis=-1))
        if target_cs is Geogr2DLocation:
            return Geogr2DLocation.from_array(self.array[..., :2])


def radarpolar_to_geogr2d(phi, r, radar_center, radar_height, radar_elevation,
                          radar_angular_res, r_earth=6371000):
    """convert polar coordinates from radar data to geographic 2d
    latitude and longitude and store it in Geogr2DLocation object.
    !phi and r indicate upper left corner of pixels. returned locations
    indicate center of pixels!

    Parameters
    ----------
    r : float
        radius
    phi : float
        azimuth
    radar_center : List[float]
        [longitude, latitude] position of the radar
    radar_height : float
        height above sea level (m)
    radar_elevation : float
        beam elevation (deg)
    radar_angular_res : float
        angular resolution of data (deg)
    r_earth : Optional[float]
        earth radius

    Returns
    -------
    locations : Geogr2DLocation object
        object containing latitude, longitude coordinates
    """
    def model43(rad_height, elevation, distance, r_earth):
        """calculates the radar beam height and its projection on the ground. assumes
        getting distance at end of bins, returns distance for center of bins.
        uses a 4/3 earth equivalent model to acount the earth curvature and the
        beam refraction in the atmosphere.
        For details see ref: R. J. Doviak, D. S. Zrnic.
        "Doppler radar and weather observations".
        Academic Press, Inc. 1992.
        adapted from an IDL program from Xavi Llort and David Forcadell (david(AT)grahi.upc.edu)

        Parameters
        ----------
        rad_height : float
            altidute of radar (m)
        elevation : float
            angle of elevation (radians)
        distance : np.ndarray
             distance from radar along a ray, end of bin (m)
        r_earth : float
            earth radius (m)

        Returns
        -------
        distance_ground : np.ndarray
            distance of bins from radar over ground, center of bin (m)
        beam_height : np.ndarray
            height of bins (m)
        """
        ### equivalent earth radius
        r_equi = (4.*r_earth) / 3.

        cos_elevation = np.cos(np.radians(elevation))
        sin_elevation = np.sin(np.radians(elevation))

        distance_ground = np.empty_like(distance)
        beam_height = np.empty_like(distance)

        sin_elevation = np.ones_like(distance) * sin_elevation
        cos_elevation = np.ones_like(distance) * cos_elevation

        ### insert 0.m distance before range 0 (ca. 60.m)
        distance = np.insert(distance, 0, 0., axis=1)
        ### calculate center of range bins
        distance[:,1:] = distance[:,1:] - (distance[:,1:] - distance[:,:-1])/2.
        distance_ground[:,:] = np.arctan(
            distance[:,1:] *
            cos_elevation / (distance[:,1:]*sin_elevation+r_equi+rad_height)
        ) * r_equi

        beam_height[:,:] = np.sqrt(
            (r_equi+rad_height)**2. +
            distance[:,1:]**2. +
            2.*(r_equi+rad_height)*distance[:,1:]*sin_elevation
        ) - r_equi
        return distance_ground, beam_height

    r = np.asarray(r)
    phi = np.asarray(phi)
    ### radar beam height and projection on ground using 4/3 earth radius
    ### equivalent
    distance_ground, height = model43(radar_height, radar_elevation, r, r_earth)
    ### width of half beam resolution
    half_beamwidth = radar_angular_res/2.
    ### convert azimuth to polar angle and get angle of pixel center
    angle = 90. - phi - half_beamwidth
    angle[angle < 0.] += 360.
    angle = np.radians(angle)
    ### get distance from radar center (in x and y directions)
    x = distance_ground * np.cos(angle)
    y = distance_ground * np.sin(angle)
    ### add lat lon offset to radar center
    lon = (radar_center[0] + np.degrees(x / r_earth) /
           np.cos(np.radians(radar_center[1])))
    lat = (radar_center[1] + np.degrees(y / r_earth))
    return Geogr2DLocation(lat, lon)


class Torus2DLocation(Cartesian2DLocation):
    def _calc_distance(self, x):
        dx = np.min([np.abs(self.array - x.array),
                     1.0 - np.abs((self.array - x.array))],
                    axis=0)
        return np.sqrt((dx**2).sum(axis=-1))


class PeriodicIndexLocation(IndexLocation):
    """
    One-dimensional, perdiodic domain (ie. a circle) where locations are
    just numbered from 0 to n-1.

    """
    totalsize = None

    def __init__(self, index, **loc_kw_args):
        ### total size = size of Lorenz 96, required for distance calculation
        try:
            self.set_circle_size(loc_kw_args.pop('totalsize'))
        except KeyError:
            pass
        super().__init__(index, **loc_kw_args)

    ### need to set as a class attribute to avoid it in the constructor (quick
    ### solution due to problems when getting the closest indices)
    @classmethod
    def set_circle_size(cls, size):
        if cls.totalsize is None:
            cls.totalsize = size
            logging.debug("PeriodicIndexLocation.totalsize set to {0}.".format(size))
        else:
            if size != cls.totalsize:
                raise ValueError(("PeriodicIndexLocation.totalsize has already "
                                  "been set to {0}. Cannot set it to {1}.".format(cls.totalsize, size)
                                  ))

    def _calc_distance(self, x):
        """
        Distance is the minimum of clockwise or counter-clockwise distance
        between indices.

        """

        return np.fmin(np.abs(self.array - x.array),
                       self.totalsize - np.abs(self.array - x.array)
                       )
