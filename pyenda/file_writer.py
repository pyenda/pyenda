#!/usr/bin/env python
# -*- coding: utf-8 -*-

# authors:
# * Geppert, Gernot <gernot.geppert@uni-hamburg.de>
# * Merker, Claire <claire.merker@uni-hamburg.de>


"""
NetCDF File Output
------------------

contains classes handling netcdf output writing

Version
-------
* 2015-12-01

To Do
-----
* comment functions

"""

import netCDF4 as nc
import datetime
import numpy as np
import logging

_unixstart = datetime.datetime(1970,1,1)


class Variable(object):
    """class to collect netCDF variable information passed to FileWriter.

    Parameters
    ----------
    name : str
        variable name (lower case - cf conventions)
    dtype : str
        netcdf variable type
    dimensions : tuple[str]
        variable's dimension names (lower case - cf conventions)
    unit : str
        variable unit
    digits : int
        number of digits of precision for netcdf4 compression. None if not
        specified

    Attributes
    ----------
    name : str
        variable name
    dtype : str
        netcdf variable type
    dimensions : tuple[str]
        variable's dimension names
    unit : str
        variable unit
    digits : int
        number of digits of precision for netcdf4 compression. None if not
        specified
    """
    def __init__(self, name, dtype, dimensions, unit, digits=None):
        self.name = name
        self.dtype = dtype
        self.dimensions = dimensions
        self.unit = unit
        self.digits = digits

    def add_time_dim(self):
        ### assumes 'time' is not already in dimensions!
        return Variable(self.name, self.dtype,
                        ('time',) + self.dimensions,
                        self.unit, self.digits)

    def add_members_dim(self):
        ### assumes that if 'time' is already in dimensions, it is the first dim
        ### and assumes that 'members' it not already in dimensions
        if 'time' in self.dimensions:
            dimensions = ('time', 'members') + self.dimensions[1:]
        else:
            dimensions = ('members',) + self.dimensions
        return Variable(self.name, self.dtype,
                        dimensions, self.unit, self.digits)

    def add_coord_dims(self, coord_dims):
        ### assumes that if 'time' is already in dimensions, it is the first dim
        ### and that if 'members' is already in dimensions, that it is the first
        ### or the second dim
        if 'time' in self.dimensions:
            if 'members' in self.dimensions:
                dimensions = ('time', 'members') + tuple(coord_dims) + self.dimensions[2:]
            else:
                dimensions = ('time',) + tuple(coord_dims) + self.dimensions[1:]
        else:
            if 'members' in self.dimensions:
                dimensions = ('members',) + tuple(coord_dims) + self.dimensions[1:]
            else:
                dimensions = tuple(coord_dims) + self.dimensions
        return Variable(self.name, self.dtype, dimensions, self.unit, self.digits)


def read_netcdf(file_name):
    """read data from netcdf file

    Returns
    -------
    dict
        dictionary containing variable names and data
    """
    pass


def datetime_to_unixtime(dt):
    try:
        iter(dt)
        times = np.asarray(dt)
    except:
        return (dt-_unixstart).total_seconds()
    return np.array([t.total_seconds() for t in times-_unixstart])


def unixtime_to_datetime(ut):
    try:
        iter(ut)
        times = np.asarray(ut)
    except:
        return datetime.datetime.utcfromtimestamp(ut)
    return np.array([datetime.datetime.utcfromtimestamp(t) for t in times])


class Writer(object):
    """base class for writing data to netcdf files

    Parameters
    ----------
    file_name : str
        file path and name
    variables : list[Variable object]
        instances of Variable class with type description and dimension for netcdf file
        creation
    dimensions : list[tuple]
        required dimension names and sizes for netcdf file creation
        - tuples must be (name, size)
        - name: str - size: int or None
    add_time_dim : Optional[bool]
        True or False
    add_coord_dims : Optional[list[tuple]]
        list of dimension tuples for coordinates to add to variables

    Attributes
    ----------
    _cnt = int
        index for time dimension to assign data to correct location when
        appending
    _fid : NetCDF.Dataset object
        netcdf file id
    """
    def __init__(self, file_name, variables, dimensions,
                 add_time_dim=False, add_coord_dims=None):
        """constructor"""
        self._cnt = 0
        try:
            self._fid = nc.Dataset(file_name, 'w', clobber=False)
        except:
            logging.error(
                "Could not open {0} in Writer.__init__".format(file_name))
            raise

        if add_coord_dims is not None:
            variables = self._add_coord_dims_to_variables(variables, add_coord_dims)

        ### add time dim after coord dims because it also creates the 'time' variable
        ### which should not have coord dims
        if add_time_dim:
            variables = self._add_time_dim_to_variables(variables)

        ### create dimensions
        for dim in dimensions:
            self._fid.createDimension(*dim)

        ### create variables
        for var in variables:
            v = self._fid.createVariable(var.name, var.dtype, var.dimensions,
                                         zlib=True,
                                         least_significant_digit=var.digits)
            v.unit = var.unit

    def _add_time_dim_to_variables(self, variables):
        ### add 'time' dimension
        self._fid.createDimension('time', None)
        variables = [var.add_time_dim() for var in variables]

        ### add 'time' variable
        variables = variables + [Variable('time', 'd', ('time',), 'seconds since 1970-01-01 00:00:00.0')]
        return variables

    def _add_coord_dims_to_variables(self, variables, add_coord_dims):
        for dim in add_coord_dims:
            self._fid.createDimension(*dim)
        variables = [var.add_coord_dims(list(zip(*add_coord_dims))[0]) for var in variables]
        return variables

    def write_variables(self, variables, time):
        """write data to file

        Parameters
        ----------
        variables : dict
            dictionary containing variable key and data
        time : Datetime object or list[Datetime object]
            If a single datetime, assume data arrays have no time dimension and
            write to single time index. If list, assume data arrays have time
            dimension and write to range of time indices.
        """
        try:
            length = len(time)
        except TypeError:
            length = 1
            ### add time dimension with length 1
            time = [time]
            for key, field in variables.items():
                variables[key] = np.expand_dims(field, axis=0)

        ### convert datetime objects to unix time
        time = datetime_to_unixtime(time)
        ### assign data
        self._fid['time'][self._cnt:self._cnt+length] = time
        for key, field in variables.items():
            self._fid[key][self._cnt:self._cnt+length] = field
        self._fid.sync()
        self._cnt += length

    def write_locations(self, locations):
        """
        Parameters
        ----------
        locations : Location object
            location of variable to write to netcdf file
        """

        # iterate over components of locations (last axes of
        # location.array) and write to individual variables
        for name, var in zip(locations.var_names,
                             np.rollaxis(locations.array, -1, 0)):
            v = self._fid.createVariable(name, 'd', list(zip(*locations.coord_dims))[0],
                                         zlib=True)
            v[:] = var

        # loc_names = [s[0].capitalize() for s in loc_dims]
        # loc_dims = [s[1] for s in loc_dims]
        # for loc_var, loc_name, loc_dim in zip(np.rollaxis(
        #         locations.array, -1, 0), loc_names, loc_dims):
        #     v = self._fid.createVariable(loc_name, 'd', loc_dim)
        #     v[:] = loc_var

    def add_dimensions(self, dimensions):
        for dim in dimensions:
            self._fid.createDimension(*dim)

    def add_variables(self, variables):
        for var in variables:
            v = self._fid.createVariable(var.name, var.dtype, var.dimensions,
                                         zlib=True,
                                         least_significant_digit=var.digits)
            v.unit = var.unit

    def close_file(self):
        """close netcdf file"""
        self._fid.close()


class EnsembleWriter(Writer):
    """class for writing ensembles to netcdf files"""
    def __init__(self, file_name, variables, dimensions, ens_size,
                 add_time_dim=False, add_coord_dims=None):
        """constructor"""
        variables = [var.add_members_dim() for var in variables]
        super(EnsembleWriter, self).__init__(file_name, variables,
                                             dimensions+[('members', ens_size)],
                                             add_time_dim, add_coord_dims)

    def write_variables(self, variables, time):
        try:
            ### time is a vector and we assume the first dim of each variable
            ### field is time, then make the last dim the second
            ### dim ('time', ..., 'members') -> ('time', 'members', ...)
            length = len(time)
            for key, field in variables.items():
                variables[key] = np.rollaxis(field, -1, 1)
        except TypeError:
            ### otherwise roll to position 0 and then add 'time' dim
            length = 1
            for key, field in variables.items():
                variables[key] = np.rollaxis(field, -1, 0)
            ### add time dimension with length 1
            time = [time]
            for key, field in variables.items():
                variables[key] = np.expand_dims(field, axis=0)

        super(EnsembleWriter, self).write_variables(variables, time)


class FlaggedEnsembleWriter(EnsembleWriter):
    """class for writing ensembles with forecast/analysis flags to netcdf files

    Parameters
    ----------
    ens_size : int
        ensemble size
    """
    def __init__(self, file_name, variables, dimensions, ens_size,
                 add_time_dim=False, add_coord_dims=None):
        """constructor"""
        super(FlaggedEnsembleWriter, self).__init__(file_name, variables,
                                                    dimensions, ens_size,
                                                    add_time_dim, add_coord_dims
                                                    )

        ### create variable for flag and set attributes
        fa_flag = self._fid.createVariable('fa_flag', 'i', ('time',))
        fa_flag.long_name = 'flag indicating whether data is forecast or analysis'
        fa_flag.flag_masks = 0, 1
        fa_flag.flag_meanings = 'forecast analysis'
        #fa_flag.setncatts({'flag_masks': 0, 1,
        #                   'flag_meanings': 'forecast analysis'})

    def write_variables(self, variables, time, flag):
        """write data to file

        Parameters
        ----------
        variables : dict
            dictionary containing variable key and data
        time : float
            time stamp
        flag : int
            0: forecats, 1: analysis
        """
        self._fid['fa_flag'][self._cnt] = flag
        super(FlaggedEnsembleWriter, self).write_variables(variables, time)


class ReopenEnsembleWriter(EnsembleWriter):
    """
    Class that closes its netCDF file after every write and reopens for the next
    one instead of leaving it open.

    Parameters
    ----------
    ens_size : int
        ensemble size
    """

    def __init__(self, file_name, variables, dimensions, ens_size,
                 add_time_dim=False, add_coord_dims=None):
        """constructor"""

        super(ReopenEnsembleWriter, self).__init__(file_name, variables,
                    dimensions, ens_size, add_time_dim, add_coord_dims)

        self._filename = file_name
        self.close_file()

    def write_locations(self, locations):
        self._fid = nc.Dataset(self._filename, 'a')
        super(ReopenEnsembleWriter, self).write_locations(locations)
        self.close_file()

    def write_variables(self, variables, time):
        self._fid = nc.Dataset(self._filename, 'a')
        super(ReopenEnsembleWriter, self).write_variables(variables, time)
        self.close_file()


class DummyWriter(object):
    """nothing to write but class needed"""
    def write_variables(self, obs_dict, time):
        pass

    def write_locations(self, locations):
        pass
