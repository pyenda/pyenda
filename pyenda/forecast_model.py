#!/usr/bin/env python
# -*- coding: utf-8 -*-

# authors:
# * Merker, Claire <claire.merker@uni-hamburg.de>
# * Geppert, Gernot <gernot.geppert@uni-hamburg.de>


"""
Forecast Models
---------------

contains forecast models


Version
-------
* 2015-11-12 - initial version, including LinSystem
* 2015-01-12 - new: APEX forecast model
"""


from .file_writer import datetime_to_unixtime, Variable
from .location import NoLocation

from abc import ABCMeta, abstractmethod
import numpy as np
from scipy import integrate
import logging


class ForecastModelMeta(ABCMeta):
    """ metaclass that implements the read-only class properties domains and
    locations

    (see http://stackoverflow.com/questions/3203286/
    how-to-create-a-read-only-class-property-in-python)"""

    @property
    def domains(cls):
        # try if iter works, then have an iterable already,
        # otherwise the method call has to return an iterable
        try:
            return iter(cls.domain_location_indices)
        except TypeError:
            try:
                return iter(cls.domain_location_indices())
            except TypeError:
                logging.error(("{0}.domain_location_indices needs to "
                               "be a classmethod or staticmethod. ".format(cls)
                               )
                              )
                raise

    @property
    def locations(cls):
        # try calling filter_locations(), which should return a Location
        # if this fails, return filter_locations which should then be a
        # Location object
        try:
            return cls.filter_locations()
        except TypeError:
            try:
                return cls.filter_locations
            except TypeError:
                logging.error(("{0}.filter_locations needs to "
                               "be a classmethod or staticmethod. ".format(cls)
                               )
                              )
                raise


class ForecastModel(object, metaclass=ForecastModelMeta):
    """base class for forecast models

    Parameters
    ----------
    name : str
        model instance name
    cfg : dict
        model configuration
    initialize : Optional[bool]
        if True, initialize the model's state

    Class Attributes
    ----------------
    These need to be defined by derived classes:

    kind =
    variables =

    kind : str
        model description
    variables : List[Variable object]
        instances of Variable class with: name, dtype, dimensions, units,
        number of digits of precision if specified
        - Variable dimensions empty when class initialised!

    These can optionally be defined by derived classes (needed for auxiliary
    file output):

    aux_dimensions : List[tuple]
        only needed if file output of auxiliary variables (that are not in
        ForecastModel.get_filter_state()/ForecastModel.deconstruct()) is required.
        auxiliary variables are not used for data assimilation and can have
        dimensions different from ForecastModel.deconstruct() output (indicated in
        ForecastModel.variables). indicate if auxiliary variables should be
        written to file in pyenda/config/assimilate_<>.yml
        - ! contains only variable dimensions other than location dimensions !
        - required dimension names and sizes for auxiliary netcdf file creation
        - tuples must be (name, size)
        - name: str -  size: int or None
    aux_variables : List[Variable object]
        only needed if file output of auxiliary variables (that are not in
        ForecastModel.get_filter_state()/ForecastModel.deconstruct()) is required.
        auxiliary variables are not used for data assimilation and can have
        dimensions different from ForecastModel.deconstruct() output (indicated in
        ForecastModel.variables). indicate if auxiliary variables should be
        written to file in pyenda/config/assimilate_<>.yml
        - instances of Variable class with: name, dtype, dimensions, units,
          number of digits of precision if specified
        - Variables dimensions empty when class initialised!
    aux_locations : Location object
        only needed if file output of auxiliary variables (that are not in
        ForecastModel.get_filter_state()/ForecastModel.deconstruct()) is required.
        auxiliary variables are not used for data assimilation and can have
        dimensions different from ForecastModel.deconstruct() output (indicated in
        ForecastModel.variables). indicate if auxiliary variables should be
        written to file in pyenda/config/assimilate_<>.yml
        - locations of the auxiliary variables (ForecastModel.aux_variables),
          dimensions corresponding to output of ForecastModel.aux_deconstruct()
        - has to be implemented together with the property
          ForecastModel.aux_locations and the method ForecastModel.aux_deconstruct(),
          analogous to ForecastModel.locations, ForecastModel.filter_locations,
          ForecastModel.deconstruct()

    These have default implementations:

    dimensions : List[tuple]
        - ! contains only variable dimensions other than location dimensions !
        - required dimension names and sizes for main netcdf file creation
        - tuples must be (name, size)
        - name: str -  size: int or None
        - default: []
    filter_locations : Location object
        - locations of the filter state (shape does not matter);
          ForecastModel.get_filter_state() and ForecastModel.update_state()
          have to handle indices which are meant to index the filter_locations
          array
        - a derived subclass has to provide a class attribute or a
          classmethod/staticmethod (note that it can also be set
          as a class attribute in any initialization method (init_model,
          init_ensemble, ...)
        - returned by property ForecastModel.locations
        - default: NoLocation([1])
    domain_location_indices : List[key]
        - each key is used to index filter_locations to retrieve the
          locations for a local domain; these domains have to be disjunct
          * a key can be an integer if filter_locations is 1D and each domain
            has one location:
            domain_location_indices = [0, 1, 2, 3]
          * a key can be a list of integers if filter_locations is 1D and each
            domain has several locations:
            domain_location_indices = [[0, 1], [1, 2], [2, 3]]
          * a key can be a tuple suitable for indexing filter_locations:
            domain_location_indices = [(0, 0), (0, 1), (1, 0), (1, 1)]
            (one location each from a 2D location object)
            domain_location_indices = [([0, 0, 1, 1], [0, 1, 0, 1]),
                                       ([0, 0, 1, 1], [1, 2, 1, 2]),
                                       ([0, 0, 1, 1], [2, 3, 2, 3])]
            (four locations each from a 2D location object
          * a key can thus be the result of a np.where call
        - a derived subclass can override the class attribute with a new
          attribute or a classmethod/staticmethod (note that it can also be set
          as a class attribute in any initialization method (init_model,
          init_ensemble, ...)
        - default: one location per domain (one grid point)
    """

    __metaclass__ = ForecastModelMeta

    dimensions = []
    filter_locations = NoLocation([1])

    def __init__(self, name, cfg, initialize=True):
        """constructor"""
        self.name = name
        if initialize:
            self.init_model(cfg)

    @classmethod
    def domain_location_indices(cls):
        """default for domain_location_indices returns a list of domains
        with one location each

        cls.locations is definded in the metaclass ForecastModelMeta and
        uses cls.filter_locations which has to be implemented by the
        derived class"""
        return list(zip(*[np.expand_dims(a.flat, -1) for a in np.indices(cls.locations.shape)]))

    @classmethod
    @abstractmethod
    def init_ensemble(cls, cfg, ensmbl_size):
        """abstract method, create first ensemble
        returns list of model instances (each instance is one ensemble member)"""

    @abstractmethod
    def init_model(self, cfg):
        """abstract method, initialise model without ensemble
        sets self.state, needs cfg dict"""

    @abstractmethod
    def forecast(self, t, dt):
        """abstract method, one forecast step
        update self.state"""

    @abstractmethod
    def deconstruct(self):
        """deconstruct filter state vector to match ForecastModel.locations.shape
        for netcdf output

        IMPLEMENT THIS METHOD TO GET WHAT YOU WANT WRITTEN TO FILE

        Returns
        -------
        dict
            dictionary containing all filter state variables reshaped to the
            correct dimensions (ForecastModel.locations.shape) for netcdf output.
            - the output dimensions have to match ForecastModel.locations.shape
            (used to create the dimensions in the output file in file_writer.FileWriter)
            - the dict keys have to match the ones in ForecastModel.variables
        """
        # return {'state': self.get_filter_state().reshape(self.locations.shape, order='C')}

    def aux_deconstruct(self):
        """optional, only needed if file output of auxiliary variables (that are
        not in ForecastModel.get_filter_state()/ForecastModel.deconstruct()) is
        required. auxiliary variables are not used for data assimilation and can
        have dimensions different from ForecastModel.deconstruct() output (indicated
        in ForecastModel.variables). indicate if auxiliary variables should be
        written to file in pyenda/config/assimilate_<>.yml.

        deconstruct model variables to match ForecastModel.aux_locations.shape
        for netcdf output

        IMPLEMENT THIS METHOD TO GET WHAT YOU WANT WRITTEN TO FILE

        Returns
        -------
        dict
            dictionary containing any model variables reshaped to the
            correct dimensions (ForecastModel.aux_locations.shape) for netcdf output.
            - the output dimensions have to match ForecastModel.aux_locations.shape
            (used to create the dimensions in the output file in file_writer.FileWriter)
            - the dict keys have to match the ones in ForecastModel.aux_variables
        """

    @abstractmethod
    def get_filter_state(self, index=slice(None)):
        """return part of state vector considered for assimilation as 1d array
        ('filter state'). implement this method so that index (dimensions
        correspond to filter_locations) allows for selecting the corresponding
        slices of 'filter state'

        Parameters
        ----------
        index : Tuple[List[int]]
            index of elements to select in the 'filter state'

        here: the whole model state vector is passed to the assimilation"""
        #return self.state[index]

    @abstractmethod
    def update_state(self, new_filter_state, index=slice(None)):
        """update parts of the assimilated part of the state vector
           (used in LETKF to update local domains)

        Parameters
        ----------
        new_filter_state: np.ndarray
            filter_state variables to update at positions corresponding to
            index
        index : List[int]
            indices of elements to update in 'filter state'! (dimensions correspond
            to filter_locations)

        here: use the given index to update the according elements of the
        model's state vector (all model state is used for assimilation)."""
        #self.state[index] = new_filter_state
