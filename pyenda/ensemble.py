#!/usr/bin/env python
# -*- coding: utf-8 -*-

# authors:
# * Merker, Claire <claire.merker@uni-hamburg.de>


"""
Ensembles
---------

contains ensemble classes and handles parallelisation of ensemble forecast runs

Version
-------
* 2015-11-12
"""


import numpy as np
# import pp
import logging


def _forecast_for_pp(model, t, dt):
    """Call forecast method for one ForecastModel instance (parallelised)"""
    model.forecast(t, dt)
    return model


class Ensemble(object):
    """store ensemble

    Attributes
    ----------
    _name : str
        ensemble description
        - has Getter method
    _members : np.ndarray
        2d array containing the part of the state vector considered by the
        data assimilation for all ensemble members
        - 1st dim: size of the filtered state vector (only elements considered
        by the assimilation)
        - 2nd dim: ensemble size
        - has Getter and Setter methods

    Parameters
    ----------
    name : str
        description, model used for ensemble
    ensemble : np.ndarray
        ensemble of state vectors, 1st dim: member size, 2nd dim: ensemble size
    """
    def __init__(self, name, ensemble):
        """constructor"""
        self._name = name
        self._members = np.atleast_2d(ensemble)

    @property
    def name(self):
        return self._name

    @property
    def members(self):
        """get ensemble members"""
        return self._members

    @members.setter
    def members(self, new_ensemble):
        """update ensemble members"""
        self._members = np.atleast_2d(new_ensemble)

    @property
    def size(self):
        """get ensemble size (int)"""
        return self.members.shape[1]

    @property
    def mean(self):
        return self._mean

    def get_mean_perturbs(self, domain):
        mean = self.members[domain].mean(axis=1)
        return mean, (self.members[domain].T - mean).T

    def get_covariance(self):
        """ensemble covariance (np.ndarray)"""
        return np.cov(self.members, ddof=1)

    def inflate(self):
        """ensemble inflation"""
        return None


class ModelEnsemble(Ensemble):
    """Initializes a list of instances of ForecastModel, each with its own
    state.

    Attributes
    ----------
    _model_members : list[ForecastModel object]
        list of ForecastModel (model_class) instances, one instance per ensemble
        members
    _nproc : int
         number of processors
    model_class : class
        ForecastModel class to use as forecast model for ensemble generation

    Parameters
    ----------
    name : str
        description, model used for ensemble
    ensemble_size : int
        required size of ensemble
    model_class : class in enfk/forecast_model module
        ForecastModel class to use as forecast model for ensemble generation
    model_config : dict
        model configuration parameters
    nproc : int
        number of processors. 0: no parallel jobs started, 1: one parallel job,
        etc...
    """
    def __init__(self, name, ensemble_size, model_class, model_config, nproc):
        """constructor"""
        self._name = name
        self.model_class = model_class
        self._nproc = nproc
        # if self.nproc > 0:
        #     self.ppserver = pp.Server(self.nproc)

        self._model_members = model_class.init_ensemble(model_config, ensemble_size)

    def __del__(self):
        if self.nproc > 0:
            self.ppserver.destroy()

    @property
    def nproc(self):
        return self._nproc

    @property
    def size(self):
        return len(self._model_members)

    def get_mean_perturbs(self, domain):
        def local_filter_state_iter(domain):
            for member in self._model_members:
                yield member.get_filter_state(domain)

        ensemble = np.vstack([filter_state for filter_state
                            in local_filter_state_iter(domain)]).T
        mean = ensemble.mean(axis=1)
        return mean, (ensemble.T - mean).T

    def __iter__(self):
        for member in self._model_members:
            yield member.get_filter_state(slice(None))

    @property
    def members(self):
        """
        Array containing state of all members
        """
        return np.vstack([filter_state for filter_state in self]).T

    @members.setter
    def members(self, new_ensemble):
        """
        Set the states of the ForecastModel instances to the values of the new
        ensemble
        """
        for member, new_filter_state in zip(self._model_members, new_ensemble.T):
            member.update_state(new_filter_state)

    def get_local_ensemble(self, indices):
        return Ensemble("local ensemble",
                        np.vstack([member.get_filter_state(indices)
                                  for member in self._model_members]).T
                        )

    def set_local_ensemble(self, new_ensemble, indices):
        for member, new_filter_state in zip(self._model_members, new_ensemble.T):
            member.update_state(new_filter_state, indices)

    def forecast(self, t, dt):
        """
        Calls the forecast method of each of the ForecastModel instances.

        """

        if self.nproc == 0:
            cnt = 0
            for member in self._model_members:
                ### logging info
                logging.debug('  - member %s', cnt+1)
                member.forecast(t, dt)
                cnt += 1
        else:
            raise NotImplementedError
            # jobs = []
            # for model in self._model_members:
            #     jobs.append(self.ppserver.submit(_forecast_for_pp, (model, t, dt),
            #                                      modules=('pyenda.forecast_model',
            #                                               model.__module__)
            #                                 )
            #                 )
            # self.ppserver.wait()
            # ### use loop instead of list comprehension to save memory
            # for i, job in enumerate(jobs):
            #     ### logging info
            #     logging.debug('  - member %s', i+1)
            #     self._model_members[i] = job()

    def deconstruct(self):
        """Calls the deconstruct method of each of the ForecastModel instances
        and combines the results into one dict.
        """
        return self.core_deconstruct('deconstruct')

    def aux_deconstruct(self):
        """Calls the aux_deconstruct method of each of the ForecastModel instances
        and combines the results into one dict.
        """
        return self.core_deconstruct('aux_deconstruct')

    def core_deconstruct(self, deconstruct_method):
        """Calls deconstruct_method of each of the ForecastModel instances
        and combines the results into one dict."""
        out = {}

        ### initialise output with correct dimensions:
        ### field dimensions + ensemble size
        ### getattr calls indicated deconstruct_method for ForecastModel class
        for key, value in getattr(self._model_members[0],
                                  deconstruct_method)().items():
            out[key] = np.full(value.shape + (self.size,), np.nan)

        ### deconstruct field for every ensemble member
        ### getattr calls indicated deconstruct_method for ForecastModel class
        for i, model_member in enumerate(self._model_members):
            model_out = getattr(model_member, deconstruct_method)()
            for key in out.keys():
                out[key][..., i] = model_out[key]
        return out

    def local_ensemble(self, indices):
        """Calls ForecastModel.local_filter_state() for each instance
        and combines the results into one Ensemble object

        Parameters
        ----------
        indices : List[int]
            list of indices of locations (included in ForecastModel.locations)
            for which to return the local state variables. the indices have to
            be the indices of the wanted locations in Location.array()
        """
        ### initialise output with correct dimensions:
        ### indices dimension + ensemble size
        out = np.full(indices.size + self.size, np.nan)

        ### get localised filter state for every ensemble member
        for i, model_member in enumerate(self.model_members):
            local[:, i] = model_member.filter_state[indices]

        return out

    def get_covariance(self):
        raise NotImplementedError


class ObservationEnsemble(Ensemble):
    """Holds an observation ensemble (analysis equivalent ensemble) and allows
    its deconstruction into sensible fields according to the used observation
    operator from pyenda/observation_operator module.

    Attributes
    ----------
    obs_operator_class : class in pyenda/observation_operator module
        observation operator class to use to deconstruct observation ensemble

    Parameters
    ----------
    name : str
        description, model used for ensemble
    ensemble : np.ndarray
        ensemble of state vectors, 1st dim: member size, 2nd dim: ensemble size
    obs_operator_class : class in pyenda/observation_operator module
        observation operator class to use to deconstruct observation ensemble
    """
    def __init__(self, name, ensemble, obs_operator_class):
        """constructor"""
        self.obs_operator_class = obs_operator_class
        super(ObservationEnsemble, self).__init__(name, ensemble)

    def deconstruct(self, output_shape):
        """Calls the deconstruct method of self.obs_operator_class for each of
        the observation ensemble members and combines the results into one dict.

        Parameters
        ----------
        output_shape : tuple
            original input shape of observations (deconstruct observation ensemble
            to original shape for file output)
        """
        out = {}

        ### initialise output with correct dimensions:
        ### field dimensions + ensemble size
        key_list = [var.name for var in self.obs_operator_class.variables]
        for key in key_list:
            out[key] = np.full(output_shape + (self.size,), np.nan)

        ### deconstruct field for every ensemble member
        for i, obs_member in enumerate(self.members.T):
            obs_out = self.obs_operator_class.deconstruct(obs_member,
                                                          output_shape)
            for key in out.keys():
                out[key][..., i] = obs_out[key]
        return out
