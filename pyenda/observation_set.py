#!/usr/bin/env python
# -*- coding: utf-8 -*-

# authors:
# * Geppert, Gernot <gernot.geppert@uni-hamburg.de>


"""
Observation Sets
----------------

contains classes handling observation sets

an observation set is a set of regularly spaced (in time) observation of one type

Version
-------
* 2016-04-20

To Do
-----
* comment...
"""


from .error_models import error_models
from . import observation_operator
from .observation import (select_obs_from_file)
from .file_writer import (ReopenEnsembleWriter,
                              DummyWriter)

import numpy as np
import datetime as dt
import logging


end_of_time = dt.datetime(9999, 1, 1)


def create_observation_set(obs_set_dict, model,
                           write_obs_ensemble=False, ensemble_size=0):
    """creates AssimilationSet object

    Parameters
    ----------
    obs_set_dict : dict
        contains information needed to create an observation set, see
        observations/template.yml for more details
    model : class
        forecast model class (models/)
    write_obs_ensemble : Bool
        True: observation equivalent ensemble is written to file
    ensemble_size : int
        size of model ensemble from which to compute observation equivalent ensemble

    Returns
    -------
    obs_set : AssimilationSet object
    """
    ### get observation operator for observation
    current_op = getattr(observation_operator, obs_set_dict['operator'])()
    # obsset has to contain locations and file_ind, so call
    # _select_obs_from_file here and store these in the obsset

    ### get index of observation in data file and locations
    ind, obs_locations = select_obs_from_file(obs_set_dict['file'],
                                              current_op.variables[0].name,
                                              **obs_set_dict['location'])

    ### write locations of observations in netcdf file if observation
    ### ensemble has to be stored
    if write_obs_ensemble:
        current_writer = ReopenEnsembleWriter(obs_set_dict['obs_ens_out_file'],
                                              current_op.variables,
                                              current_op.dimensions,
                                              ensemble_size,
                                              add_time_dim=True,
                                              add_coord_dims=obs_locations.coord_dims
                                              )
    else:
        current_writer = DummyWriter()

    current_writer.write_locations(obs_locations)

    ### store original input observation shape
    input_shape = obs_locations.shape

    ### get filter state index closest to observation location
    obs_locations = obs_locations.reshape(-1)
    filter_state_ind = current_op.get_filter_state_indices(
        model.locations, obs_locations)

    ### add observation to observation set for data assimilation
    obs_set = AssimilationSet(current_op,
                              current_writer,
                              parse_obs_time(obs_set_dict['time']),
                              obs_locations,
                              input_shape,
                              filter_state_ind,
                              obs_set_dict['file'],
                              ind,
                              obs_set_dict['cov_file'],
                              model.locations,
                              model.domains,
                              obs_set_dict.get('radius', None)
                              )
    return obs_set


def obs_generator(obs_sets, t_end):
    try:
        ind = np.argmin(np.array([obs_set.next_obs_time for obs_set in obs_sets]))
    except ValueError:  # empty list, ie. no observations
        logging.error("Could not process observations.")
        return
    obs_time = obs_sets[ind].next_obs_time
    while obs_time < end_of_time and obs_time <= t_end:
        yield obs_time, obs_sets[ind]  # , obs_set_dicts[ind]
        obs_sets[ind].advance_time()
        ind = np.argmin(np.array([obs_set.next_obs_time for obs_set in obs_sets]))
        obs_time = obs_sets[ind].next_obs_time


def _time_generator(start, stop, step):
    t = start
    while t <= stop:
        yield t
        t += step


def parse_obs_time(obs_time):
    """
    Parse time information from observation list, which is
    either start, stop, step for a slice or a list of timestamps.

    Returns
    -------
    iterator
        Iterator that always returns the next time for this observation set.
    """
    try:
        return _time_generator(obs_time['start'], obs_time['stop'], obs_time['step'])
    except:  # assume it's a list
        return iter(obs_time)


class ObservationSet(object):
    """
    Attributes
    ----------
    operator : ObsOperator object
        observation operator
    _writer : FileWriter class
        handling netcdf file writing
    _obs_times : iterator
        iterate through observation times
    locations : Location object
        observation locations
    _input_shape : tuple
        shape of the original input observation fields

    Parameters
    ----------
    operator : ObsOperator object
        observation operator
    writer : FileWriter class
        handling netcdf file writing
    obs_times : iterator
        iterate through observation times
    locations : Location object
        observation locations
    input_shape : tuple
        shape of the original input observation fields
    """

    def __init__(self, operator, writer, obs_times, locations, input_shape):
        """constructor"""
        self.operator = operator
        self._writer = writer
        self._obs_times = obs_times
        self.next_obs_time = next(self._obs_times)
        self.locations = locations
        self._input_shape = input_shape

    @property
    def input_shape(self):
        return self._input_shape

    def advance_time(self):
        try:
            self.next_obs_time = next(self._obs_times)
        except StopIteration:
            ### set next_obs_time to a datetime object (to allow comparison to t)
            ### that is far enough into the future to be after t_end of the experiment
            self.next_obs_time = end_of_time

    def write(self, obs_ensemble, t):
        """calls self._writer.write_variables() to write observation ensemble
        to file

        Parameters
        ----------
        obs_ensemble : ObservationEnsemble object
            contains observation ensemble for time t
        t : Datetime object
            time step
        """
        self._writer.write_variables(obs_ensemble.deconstruct(self._input_shape),
                                     t)


class AssimilationSet(ObservationSet):
    """
    Attributes
    ----------
    _state_ind : Tuple[np.ndarray]
        arrays of indices in state vector
    _filename : str
        file name for observation set
    _file_ind : Tuple[np.ndarray] or Slice
        indices in file
    _cov_filename : str
        file name for observation set covariance

    Parameters
    ----------
    operator : ObsOperator object
        observation operator
    writer : FileWriter class
        handling netcdf file writing
    obs_times : iterator
        iterate through observation times
    locations : Location object
        observation locations
    input_shape : tuple
        shape of the original input observation fields
    state_ind : Tuple[int]
        index in state vector
    filename : str
        file name for observation set
    file_ind : Tuple[np.ndarray]
        ?
    cov_filename : str
        file name for observation set covariance
    model_locations : Optional[Location object]
        filter locations
    model_domains : Optional[List]
        list of local domains for LETKF
    radius : Optional[float]
        max distance of obs from state for LETKF

    """

    def __init__(self, operator, writer, obs_times, obs_locations, input_shape,
                 state_ind, filename, file_ind, cov_filename,
                 model_locations=None, model_domains=None, radius=None):
        """constructor"""
        super(AssimilationSet, self).__init__(operator, writer, obs_times,
                                              obs_locations, input_shape)
        self._filename = filename
        self._cov_filename = cov_filename
        self._file_ind = file_ind
        self._state_ind = state_ind
        if radius is not None:
            self._obs_domains = self.get_obs_domains(model_locations,
                                                     model_domains,
                                                     radius
                                                     )
        else:
            self._obs_domains = None

    @property
    def filename(self):
        return self._filename

    @property
    def file_ind(self):
        return self._file_ind

    @property
    def state_ind(self):
        return self._state_ind

    @property
    def cov_filename(self):
        return self._cov_filename

    @property
    def domains(self):
        return self._obs_domains

    def get_obs_domains(self, model_locations, model_domains, radius):
        obs_domains = []
        for model_domain in model_domains:
            obs_domains.append([])
            for model_location in model_locations[model_domain]:
                obs_domains[-1] = np.unique(np.concatenate(
        [np.where(model_location.calc_distance(self.locations) <= radius)[0],
         np.asarray(obs_domains[-1], dtype=np.int)]
                                                           )
                                            )
        #logging.debug('obsdomain: {0}'.format(obs_domains))

        return obs_domains


class SyntheticSet(ObservationSet):
    """
    Attributes
    ----------
    operator : ObsOperator object
        observation operator
    writer : FileWriter object or class ???
        handling netcdf file writing
    _obs_times : iterator
        iterate through observation times
    locations : Location object
        observation locations
    _error_model : str
        error model to be used (see pyenda.errormodel module)
    _error_model_args
        list of needed arguments for error model

    Parameters
    ----------
    operator : ObsOperator object
        observation operator
    writer : FileWriter object or class ???
        handling netcdf file writing
    obs_times : iterator
        iterate through observation times
    locations : Location object
        observation locations
    error_model : str
        error model to be used (see pyenda.errormodel module)
    error_model_args
        list of needed arguments for error model
    """

    def __init__(self, operator, writer, obs_times, locations, input_shape,
                 error_model, error_model_args):
        """constructor"""
        super(SyntheticSet, self).__init__(operator, writer, obs_times, locations, input_shape)
        self._error_model = error_models[error_model]
        self._error_model_args = error_model_args

    def add_error(self, observation):
        return self._error_model(observation, self._error_model_args)

    def write(self, observation, t):
        self._writer.write_variables(self.operator.deconstruct(observation, self._input_shape),
                                     t
                                     )
