#!/usr/bin/env python
# -*- coding: utf-8 -*-

# authors:
# * Merker, Claire <claire.merker@uni-hamburg.de>
# * Geppert, Gernot <gernot.geppert@uni-hamburg.de>


"""
Observation Operators
---------------------

contains observation operator classes

Version
-------
* 2015-11-13

To Do
-----
* add required class attributes to base class
"""

from .forecast_model import Variable

from abc import ABCMeta, abstractmethod, abstractproperty
import numpy as np


class ObsOperator(metaclass=ABCMeta):
    """base class for observation operators

    Attributes
    ----------
    kind : str
        observation operator description

    Parameters
    ----------
    kind : str
        observation operator description

    Class Attributes
    ----------------
    size : int
        size of created equivalent observation vector
    dimensions : List[tuple]
        empty when class initialised, except from ObsOperators with NoLocation
        location type: has to contain index dimension!
        - required dimension names and sizes for netcdf file creation
        - tuples must be (name, size)
        - name: str -  size: int or None
    variables : List[Variable object]
        instances of Variable class with: name, dtype, dimensions, units,
        number of digits of precision if specified
        - dimensions empty when class initialised, except from ObsOperators
        with NoLocation location type: has to contain index dimension information!
    """

    size = []
    dimensions = []
    variables = []

    # def __init__(self, kind):
    #     """constructor"""
    #     self.kind = kind

    @staticmethod
    @abstractmethod
    def get_filter_state_indices(model_locations, obs_locations):
        """returns the indices of the elements of the global filter_state
        vector (with dimensions corresponding to ForecastModel.filter_locations)
        that are required to calculate the observation at obs_locations"""

    def calc_obs_ensemble(self, ensmbl, state_ind):
        """apply observation operator to model state ensemble

        Parameters
        ----------
        ensmbl : Ensemble object
            object containing ensemble members
        state_ind : tuple[np.ndarray]
            1d array with indices in filter state vector - like ([...],),
            the length of the array is the size of the obs vector
        """
        return np.vstack([self._calc_obs(filter_state[state_ind])
                          for filter_state in ensmbl]).T

    def calc_obs(self, state, state_locations, obs_locations):
        """
        Note that only the result of ForecastModel.get_filter_state() is passed
        to ObsOperator.calc_obs!
        """

        ### reshape to 1D location vector
        obs_locations = obs_locations.reshape(-1)
        ### find indices of closest locations in state_locations
        ind = obs_locations.get_closest_indices(state_locations)
        ### and use to index state (note that ind is a tuple of arrays
        ### with shape of obs_locations and that state is a vector)

        return self._calc_obs(state[ind])

    @staticmethod
    def _calc_obs(state):
        """
        method that calculates observations for the given state vector elements
        - here identity operator
        """
        return state

    @staticmethod
    def construct(obs, loc):
        """
        construct obs vector and location vector from observation variables
        and locations

        An ObsOperator can have several variables but they will share the set
        of locations, therefore loc is no dict.
        """

        return (obs['observation'].reshape(-1, order='C'),
                loc.reshape(-1, order='C')
                )

    @staticmethod
    def deconstruct(obs_vector, obs_locations_shape):
        """deconstruct obs vector into observation variables and reshape to locations"""
        return {'observation': obs_vector.reshape(obs_locations_shape, order='C')}


class IdentityOperator(ObsOperator):
    """
    Observation operator used for the Lorenz '96 and the advection examples.
    The observation is simply the state at the respective location.

    """

    kind = 'Identiy Observation Operator'
    variables = [Variable('state', 'd', (), '-')]

    @staticmethod
    def get_filter_state_indices(model_locations, obs_locations):
        """
        returns the indices of the elements of the global filter_state
        vector (with dimensions corresponding to ForecastModel.filter_locations)
        that are required to calculate the observation at obs_locations

        """

        model_locations_1d = model_locations.reshape(-1, order='C')
        ind = obs_locations.get_closest_indices(model_locations_1d)
        return ind

    @staticmethod
    def construct(obs, location):
        """
        Construct an observation vector and corresponding locations
        from a dictionary with named variables and given locations.

        """

        return (obs['state'].reshape(-1, order='C'),
                location.reshape(-1, order='C')
                )

    @staticmethod
    def deconstruct(obs_vector, obs_locations_shape):
        """
        Reshape and possibly distribute the observation vector to
        observation variables for file output.

        """

        return {'state': obs_vector.reshape(obs_locations_shape, order='C')}
