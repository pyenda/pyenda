#!/usr/bin/env python
# -*- coding: utf-8 -*-

# authors:
# * Geppert, Gernot <gernot.geppert@uni-hamburg.de>


"""
Error Models for SyntheticObsSet
--------------------------------

contains error models generating noise

Note
----
any newly implemented error model has to be added to the dictionary error_models

error_models['new model'] = new_model

Version
-------
* 2016-04-20
"""

import numpy as np

error_models = {}


def add_uncorr_gauss_error(perfect_obs, var):
    """ Add Gaussian error to observations.

    Parameters
    ----------
    perfect_obs : np.ndarray
        1d original observation
    var : float or ndarray
        given variance of error

    Returns
    -------
    np.ndarray
        observation with additive random errors
    bool
        for constant error covariances, return None
    """
    try:
        length = perfect_obs.shape[0]
    except:
        length = 1

    error = np.random.normal(0, 1, length)
    return perfect_obs + np.sqrt(var) * error, None


error_models['add_uncorr_gauss_error'] = add_uncorr_gauss_error


def _find_alpha_beta(a, b, var, tol=1e-7):
    """
    Find the Beta distribution parameters alpha and beta such that the resulting
    distribution with support [a, b] has its mode at zero and the variance var.

    The cost function is derived from the expression for the variance of the
    Beta distribution where alpha has been replaced by a expression that is
    derived from the definition of the Beta distribution and the condition
    that its maximum is at 0.

    Finds alpha, beta > 1, ie. convace pdfs.

    (taken from Gernot's thesis dart_tools.py)

    Parameters
    ----------
    a : float
        lower boundary of support of the distribution
    b : float
        upper boundary of support of the distribution
    var : float
        variance of the distribution 

    Returns
    -------
    alpha : float
        alpha parameter
    beta : float
        beta parameter
    """

    if a*b > 0:
        raise ValueError(("a has to be smaller than zero and b has to be "
                         "larger than zero."))

    ### check if the mode is inside the left half of the interval
    ### otherwise 'swap' boundaries
    swapped = False
    if abs(a) > abs(b):
        temp = a
        a = -b
        b = -temp
        swapped = True

    def cost(beta):
        test = beta * ((((b-a)*(beta-1.0))/b - beta + 2.0)
                       / ((((b-a)*(beta-1.0))/b + 2.0)**2
                          * (((b-a)*(beta-1.0))/b + 3.0)))

        return abs(test-var)

    beta = _minimize(1111, 100, cost)
    beta = _minimize(beta-1000, 10, cost)
    beta = _minimize(beta-100, 1, cost)
    beta = _minimize(beta-10, 0.1, cost)
    beta = _minimize(beta-0.9, 0.01, cost)

    alpha = ((b-a)*(beta-1.0))/b - beta + 2.0

    if swapped:
        return beta, alpha
    else:
        return alpha, beta

    return alpha, beta.x


def _minimize(start, step, costfunction):
    """
    Starting from start and increasing by step, find the argument of the cost
    function after which the function value increases (ie. the local minimum).

    Parameters
    ----------
    start : float
        starting point
    step : float
        step size
    costfunction : function
        cost function

    Returns
    -------
    float
        result from minimisation
    """
    #print start, costfunction(start), costfunction(start+step)
    while costfunction(start+step) < costfunction(start):
        #print start, costfunction(start)
        start += step

    return start


def add_uncorr_beta_error(perfect_obs, var):
    perfect_obs = np.atleast_1d(perfect_obs)
    var = np.broadcast_to(var, perfect_obs.shape)
    nditer = np.nditer([perfect_obs, var, None],
                       op_flags=[['readonly'],
                                 ['readonly'],
                                 ['allocate', 'writeonly']
                                 ]
                       )
    for perfect, error, obs in nditer:
        alpha, beta = _find_alpha_beta(0.0-perfect, 1.0-perfect, error)
        obs[...] = np.random.beta(alpha, beta)

    return nditer.operands[2], None


error_models['add_uncorr_beta_error'] = add_uncorr_beta_error
