#!/usr/bin/env python
# -*- coding: utf-8 -*-

# authors:
# * Merker, Claire <claire.merker@uni-hamburg.de>


"""
Data Assimilation Filters
-------------------------

contains data assimilation methods


Version
-------
* 2015-11-12
"""

#from .pyenda.ensemble import (Ensemble)

from abc import ABCMeta, abstractmethod
import numpy as np
from .ensemble import Ensemble
# from utils import Timer


class Filter(object):
    """base class for data assimilation filters

    Parameters
    ----------
    kind : str
        filter description

    Attributes
    ----------
    kind : str
        filter description
    """
    __metaclass__ = ABCMeta

    def __init__(self, kind):
        """constructor"""
        self.kind = kind

    @abstractmethod
    def update():
        """abstract method, update model state"""


class ETKF(Filter):
    """Ensemble Transform Kalman Filter"""
    def __init__(self):
        """constructor"""
        super(ETKF, self).__init__('Ensemble Transform Kalman Filter')

    @staticmethod
    def update(X, obs, HX):
        """ensemble update using ETKF equations (bishop et al., 2001)

        Parameters
        ----------
        X : ModelEnsemble object
            object containing an ensemble of state vectors
        obs : Observation object
            object containing the observation information
        HX : Ensemble object
            object containing an ensemble of observations obtained from a state
            ensemble using an observation operator
        """
        ### calculated analysis ensemble
        analysis = ETKF.analysis(X, obs.vector, obs.covariance, HX)

        ### this consumes too much memory! use ETKF.analysis instead
        # analysis = ETKF.analysis_arrays(X.mean,
        #                                 X.get_perturbs(),
        #                                 obs.vector, obs.covariance,
        #                                 HX.mean, HX.get_perturbs())

        ### update ensemble members
        X.members = analysis

    @staticmethod
    def analysis(X, y, R, HX,
                 model_domain=slice(None), obs_domain=slice(None)):
        """ensemble update using ETKF equations (bishop et al., 2001)
        without update of forecast ensemble member objects

        Parameters
        ----------
        X : Ensemble object
            object containing an ensemble of state vectors
        y : np.ndarray
            observation vector
        R : np.ndarray
            observation covariance matrix
        HX : Ensemble object
            object containing an ensemble of observations obtained from a state
            ensemble using an observation operator
        model_domain : tuple
            passed to ModelEnsemble.get_perturbs and on to
            ForecastModel.get_filter_state to get local domain filter state
        obs_domain : list
            used to index the observation vector
        """
        ensmbl_size = X.size

        ### ensemble perturbations update
        ### Zxa = Zxb * T
        ### T = C * [I + G]^(-1/2) * C^T
        ### Zyb^T * R^-1 * Zyb = C * G * C^T
        ### compute Zyb^T * R^-1 * Zyb for eigendecomposition
        HX_mean, HX = HX.get_mean_perturbs(obs_domain)
        Zy = 1./np.sqrt(ensmbl_size - 1) * HX

        try:
            Zy_T_Rinv = np.dot(Zy.T, np.linalg.inv(R[obs_domain][:, obs_domain]))
        except:
            print(R.shape, obs_domain)
            raise
        Zy_T_Rinv_Zy = np.dot(Zy_T_Rinv, Zy)

        ### eigendecomposition Zyb^T * R^-1 * Zyb = C * G * C^T
        ### (use variable T for matrix C)
        G, T = np.linalg.eigh(Zy_T_Rinv_Zy)

        ### if Zy_T_Rinv_Zy is not positive semidefinite (eigenvalues >= 0) then
        ### compute nearest positive semidefinite matrix (covariance matrix)
        if not np.all(G >= 0):
            #print('--- FIX MATRIX: is not positive definite !')
            ### take eigenvalues if positive or replace by 0
            G = np.maximum(G, 0)

        ### calculate matrix square-root T = C * [I + G]^(-1/2) * C^T
        ### (reuse G for result of [I + G]^(-1/2) and T was used for matrix C)
        G = np.diagflat(1.0 / np.sqrt(1.0 + G))
        T = np.dot(T, np.dot(G, T.T))

        ### update ensemble perturbations
        X_mean, X = X.get_mean_perturbs(model_domain)
        Za = np.dot(1./np.sqrt(ensmbl_size - 1) * X, T)

        ### calculate Kalman gain K = Zxa * Zya^T * R^-1
        ###                         = Zxb * T * T^T * Zyb^T * R^-1
        ###                         = Zxa * T^T * Zyb^T * R^-1
        K = np.dot(Za, np.dot(T.T, Zy_T_Rinv))

        ### calculate analysis mean xa = xb + K * [y - yb]
        ### (reuse y for vector xa)
        y = X_mean + np.dot(K, y[obs_domain] - HX_mean)

        ### update ensemble members Xa = xa + Zxa
        return (y + (np.sqrt(ensmbl_size - 1) * Za).T).T

    @staticmethod
    def analysis_arrays(X_mean, X_perturbs, y, R, HX_mean, HX_perturbs):
        """ensemble update using ETKF equations (bishop et al., 2001)
        without update of forecast ensemble member objects

        Parameters
        ----------
        X_mean : np.ndarray
            mean filter state
        X_perturbs : np.ndarray
            ensemble perturbation
        y : np.ndarray
            observation vector
        R : np.ndarray
            observation covariance matrix
        HX_mean : np.ndarray
            mean predicted observation vector
        HX_perturbs : np.ndarray
            observation ensemble perturbation
        """
        ensmbl_size = X_perturbs.shape[1]

        ### ensemble perturbations update
        ### Zxa = Zxb * T
        ### T = C * [I + G]^(-1/2) * C^T
        ### Zyb^T * R^-1 * Zyb = C * G * C^T
        ### compute Zyb^T * R^-1 * Zyb for eigendecomposition
        Zy = 1./np.sqrt(ensmbl_size - 1) * HX_perturbs
        Zy_T_Rinv = np.dot(Zy.T, np.linalg.inv(R))
        Zy_T_Rinv_Zy = np.dot(Zy_T_Rinv, Zy)

        ### eigendecomposition Zyb^T * R^-1 * Zyb = C * G * C^T
        ### (use variable T for matrix C)
        G, T = np.linalg.eigh(Zy_T_Rinv_Zy)

        ### if Zy_T_Rinv_Zy is not positive semidefinite (eigenvalues >= 0) then
        ### compute nearest positive semidefinite matrix (covariance matrix)
        if not np.all(G >= 0):
            #print('--- FIX MATRIX: is not positive definite !')
            ### take eigenvalues if positive or replace by 0
            G = np.maximum(G, 0)

        ### calculate matrix square-root T = C * [I + G]^(-1/2) * C^T
        ### (reuse G for result of [I + G]^(-1/2) and T was used for matrix C)
        G = np.diagflat(1.0 / np.sqrt(1.0 + G))
        T = np.dot(T, np.dot(G, T.T))

        ### update ensemble perturbations
        # with Timer(True, 'get perturbs') as t:
        #     X.get_perturbs()

        Za = np.dot(1./np.sqrt(ensmbl_size - 1) * X_perturbs, T)

        ### calculate Kalman gain K = Zxa * Zya^T * R^-1
        ###                         = Zxb * T * T^T * Zyb^T * R^-1
        ###                         = Zxa * T^T * Zyb^T * R^-1
        K = np.dot(Za, np.dot(T.T, Zy_T_Rinv))

        ### calculate analysis mean xa = xb + K * [y - yb]
        ### (reuse y for vector xa)
        # with Timer(True, 'get mean') as t:
        #     X.get_mean()
        y = X_mean + np.dot(K, y - HX_mean)

        ### update ensemble members Xa = xa + Zxa
        return (y + (np.sqrt(ensmbl_size - 1) * Za).T).T


class LETKF(Filter):
    """Local Ensemble Transform Kalman Filter

    Parameters
    ----------
    letkf_radius : float
        observation localisation radius

    Attributes
    ----------
    letkf_radius : float
        observation localisation radius
    """
    def __init__(self):
        """constructor"""
        super(LETKF, self).__init__('Local Ensemble Transform Kalman Filter')

    @staticmethod
    def update(X, obs, HX):
        """ensemble update using LETKF equations (hunt et al., 2007)

        analysis is done independently for local domains

        Parameters
        ----------
        X : ModelEnsemble object
            object containing an ensemble of state vectors
        obs : Observation object
            object containing the observation information
        HX : Ensemble object
            object containing an ensemble of observations obtained from a state
            ensemble using an observation operator
        """

        ### serial
        #if X.nproc == 0:

        for model_domain, obs_domain in zip(X.model_class.domains, obs.domains):
            if len(obs_domain) > 0:
                local_analysis = ETKF.analysis(X, obs.vector, obs.covariance,
                                                   HX, model_domain, obs_domain)
                    #import pdb; pdb.set_trace()
                X.set_local_ensemble(local_analysis, model_domain)
        ### parallel
        # else:
