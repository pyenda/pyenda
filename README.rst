=================================
Python Ensemble Data Assimilation
=================================

To get started, take a look at the `example <doc/examples/examples.rst>`_.

pyenda requires Python 3.x.

Developers
==========
* Gernot Geppert - University of Reading (previously at Universiät Hamburg) - g.geppert@reading.ac.uk
* Claire Merker- MeteoSwiss (previously at Universität Hamburg) - claire.merker@meteoswiss.ch

Contributors
============
* Yann Büchau - Universität Hamburg: advection example
